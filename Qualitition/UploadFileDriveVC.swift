//
//  UploadFileDriveVC.swift
//  Qualitition
//
//  Created by Callsoft on 16/10/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleAPIClientForREST
import MobileCoreServices

//NOT USED CLASS NEED TOBE DELETD

class UploadFileDriveVC: UIViewController, GIDSignInUIDelegate, UIDocumentPickerDelegate,UINavigationControllerDelegate, UIDocumentMenuDelegate {
    
    fileprivate let service = GTLRDriveService()
    private var drive: ATGoogleDrive?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnUploadFileToDrive(_ sender: Any) {
        
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeRTF), String(kUTTypePNG), String(kUTTypeJPEG), String(kUTTypeURL), String(kUTTypeText)], in: .import)
        
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        if let documentsDir = url as URL? {
            
            print("import result : \(documentsDir)")
            
            let indexFileName = (documentsDir.pathComponents.last)!
            print("THE FILE NAME", indexFileName)
            
            let testFilePath = documentsDir.appendingPathComponent("").path
            
            drive?.uploadFile("JJHHUUKK", filePath: testFilePath, MIMEType: "image/pdf") { (fileID, error) in
                print("Upload file ID: \(String(describing: fileID)); Error: \(String(describing: error?.localizedDescription))")
                
                print("THE FILE ID IS",fileID ?? "")
                print("The test File Path", testFilePath)
            }
        }
        
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = true
        } else {
            // Fallback on earlier versions
        }
        
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
}



