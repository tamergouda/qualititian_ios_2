//
//  DocumentModuleVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class DocumentModuleVC: UIViewController {

    
    
    
    //MARK:- View Life Cycle
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //MARK:- IBAction Buttons
    //MARK:-
    @IBAction func btnBackToDocument(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnHealth(_ sender: Any) {
    }
    
    @IBAction func btnFMCG(_ sender: Any) {
    }
    
    @IBAction func btnImportExport(_ sender: Any) {
    }
    
    @IBAction func btnIndustirial(_ sender: Any) {
    }
    


}
