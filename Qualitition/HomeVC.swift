//
//  HomeVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    
    //MARK:- View Life Cycle
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }



    //MARK:- Actions Buttons
    //MARK:-
    @IBAction func menuBtn(_ sender: Any) {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.drawerController.setDrawerState(.opened, animated: true)
        
        
    }
    
    @IBAction func btnUser(_ sender: Any) {
        
        let userVC = self.storyboard?.instantiateViewController(withIdentifier: "UserMangementVC") as! UserMangementVC
        
        self.navigationController?.pushViewController(userVC, animated: true)
        
        
    }
    
    @IBAction func btnDocument(_ sender: Any) {
        
        let docVC = self.storyboard?.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
        
        self.navigationController?.pushViewController(docVC, animated: true)
        
    }
    
    @IBAction func btnPrint(_ sender: Any) {
        
        let prntVC = self.storyboard?.instantiateViewController(withIdentifier: "PrintVC") as! PrintVC
        
        self.navigationController?.pushViewController(prntVC, animated: true)
        
    }
    
    
    @IBAction func btnBackUp(_ sender: Any) {
        
        let backVC = self.storyboard?.instantiateViewController(withIdentifier: "BackupVC") as! BackupVC
        
        self.navigationController?.pushViewController(backVC, animated: true)
    }
    
    
    @IBAction func btnAnalytics(_ sender: Any) {
        
        let analyticVC = self.storyboard?.instantiateViewController(withIdentifier: "AnalyticsVC") as! AnalyticsVC
        
        self.navigationController?.pushViewController(analyticVC, animated: true)
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    
    
}
