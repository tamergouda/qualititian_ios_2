//
//  AppDelegate.swift
//  Qualitition
//
//  Created by Callsoft on 07/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import DropDown
import KYDrawerController
import UserNotifications
import GoogleSignIn
import GoogleAPIClientForREST
import SwiftyDropbox


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var drawerController = KYDrawerController.init(drawerDirection: .left, drawerWidth: 275)
    var google_Auth_ClientId = "263248254071-dinoro74i9hem6a2g8rhek4oddu3pk3q.apps.googleusercontent.com"
    var orientationLock = UIInterfaceOrientationMask.portrait  // For Rotation Lock.
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        initialSetUp()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    //MARK:- Custom Functions
    //MARK:-
    func initialSetUp() {
        sleep(2)
        GIDSignIn.sharedInstance().clientID = google_Auth_ClientId  // Google Drive
        DropboxClientsManager.setupWithAppKey("ahhwmihzjb1s18u")  //Drop Box App ID
        
        IQKeyboardManager.shared.enable = true  //FOR KEYBOARD
        DropDown.startListeningToKeyboard()
        
        for_DeviceToken()  //FOR DEVICE TOKEN
        
        checkAutoLogin()
    }
    
    func checkAutoLogin() {
        
        print(UserDefaults.standard.object(forKey: "responseToken") as? String ?? "")
        
        if (UserDefaults.standard.value(forKey: "responseToken") as? String ?? "" != "")
        {
            initHome()
        }
        else {
            initLoginAtLogOut()
        }
        
    }
    
    func initHome() -> AppDelegate {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let menuVC = storyboard.instantiateViewController(withIdentifier: "Drawer")
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
        
        let navigationController = UINavigationController(rootViewController: drawerController)
        
        self.drawerController.mainViewController = homeVC
        self.drawerController.drawerViewController = menuVC
        
        navigationController.isNavigationBarHidden = true
        
        //ANIMATE THE ROOTVIEW CONTROLLER...
        UIView.transition(with: window!, duration: 0.8, options: .transitionFlipFromRight, animations: {
            self.window?.rootViewController = navigationController
        }, completion: { completed in
            // maybe do something here
        })
        
        self.window?.makeKeyAndVisible()
        
        return UIApplication.shared.delegate as! AppDelegate
        
    }
    
    func initLogin() -> AppDelegate {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let signupTypeVC = storyboard.instantiateViewController(withIdentifier: "SignUp_Type_VC")
        let navigationController = UINavigationController(rootViewController: signupTypeVC)
        
        navigationController.isNavigationBarHidden = true
        
        //ANIMATE THE ROOTVIEW CONTROLLER...
        UIView.transition(with: window!, duration: 0.8, options: .transitionFlipFromRight, animations: {
            self.window?.rootViewController = navigationController
        }, completion: { completed in
            // maybe do something here
        })
        
        self.window?.makeKeyAndVisible()
        return UIApplication.shared.delegate as! AppDelegate
        
    }
    
    func initLoginAtLogOut() -> AppDelegate {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC")
        let navigationController = UINavigationController(rootViewController: loginVC)
        
        navigationController.isNavigationBarHidden = true
        
        //ANIMATE THE ROOTVIEW CONTROLLER...
        UIView.transition(with: window!, duration: 0.8, options: .transitionFlipFromRight, animations: {
            self.window?.rootViewController = navigationController
        }, completion: { completed in
            // maybe do something here
        })
        
        self.window?.makeKeyAndVisible()
        return UIApplication.shared.delegate as! AppDelegate
        
    }
    
    func for_DeviceToken() {
        
        let simulaterToken = "Simulaterwalatokenbb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326"
        
        UserDefaults.standard.set(simulaterToken as String, forKey: "devicetoken")
        registerForRemoteNotification()
    }
    
    //MARK:- DEVICE TOKEN GET HERE
    //MARK:
    func registerForRemoteNotification() {
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    
                    DispatchQueue.main.async {
                        
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        UserDefaults.standard.set(deviceTokenString as String, forKey: "devicetoken")
        
        NSLog("Device Token : %@", deviceTokenString)
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        let simulaterToken = "Simulatorb1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326"
        UserDefaults.standard.set(simulaterToken, forKey: "devicetoken")
        print(error, terminator: "")
        
    }
    
    //MARK:- URL SCHEME MANAGEMENT For Google and DropBox
    //MARK:-
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        //For DropBox
        switch(appPermission) {
        case .fullDropbox:
            if let authResult = DropboxClientsManager.handleRedirectURL(url) {
                switch authResult {
                case .success:
                    print("Success! User is logged into DropboxClientsManager.")
                case .cancel:
                    print("Authorization flow was manually canceled by user!")
                case .error(_, let description):
                    print("Error: \(description)")
                }
            }
        case .teamMemberFileAccess:
            if let authResult = DropboxClientsManager.handleRedirectURLTeam(url) {
                switch authResult {
                case .success:
                    print("Success! User is logged into DropboxClientsManager.")
                case .cancel:
                    print("Authorization flow was manually canceled by user!")
                case .error(_, let description):
                    print("Error: \(description)")
                }
            }
        case .teamMemberManagement:
            if let authResult = DropboxClientsManager.handleRedirectURLTeam(url) {
                switch authResult {
                case .success:
                    print("Success! User is logged into DropboxClientsManager.")
                case .cancel:
                    print("Authorization flow was manually canceled by user!")
                case .error(_, let description):
                    print("Error: \(description)")
                }
            }
            
            
        }
        
        //For Google Drive
        let sourceApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
        let annotation = options[UIApplicationOpenURLOptionsKey.annotation]
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
        
        
    }  //Functions Close Bracket
    
} //Class Close Bracket


//MARK:- AUTO ORIENTATION LOCK
//MARK:-
extension AppDelegate {
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    struct AppUtility {
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }
        
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
            self.lockOrientation(orientation)
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        }
    }
    
    
}

