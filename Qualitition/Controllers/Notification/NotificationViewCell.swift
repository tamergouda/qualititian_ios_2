//
//  NotificationViewCell.swift
//  Qualitition
//
//  Created by Callsoft on 12/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class NotificationViewCell: UITableViewCell {

    
    @IBOutlet weak var imgNotificationLogo: UIImageView!
    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var lblTimeNotifi: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
