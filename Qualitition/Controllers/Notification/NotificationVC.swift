//
//  NotificationVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    
    //MARK:- IBOutlets
    //MARK:-
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- Variables
    //MARK:-
    fileprivate let webServicesConnection = AlmofireWrapper()
    fileprivate let token = UserDefaults.standard.value(forKey: "responseToken") ?? ""
    let userID = UserDefaults.standard.value(forKey: "ResponseUserID") ?? ""
    var notificationListing_Array = NSArray()
    
    
    //MARK:- View LifeCycle
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationApiCall()
    }
    
    //MARK:- View LifeCycle
    //MARK:-
    @IBAction func btnBackToHome(_ sender: Any) {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        _ = appDel.initHome()
        
    }
    
    
}


//MARK:- Table View Functions
//MARK:-
extension NotificationVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return 10
//        return notificationListing_Array.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: NotificationViewCell = tableView.dequeueReusableCell(withIdentifier: "NotificationViewCell", for: indexPath) as! NotificationViewCell
        
//        let dataDict = notificationListing_Array.object(at: indexPath.row) as? NSDictionary ?? [:]
//
//        let message = dataDict.value(forKey: "notification") as? String ?? ""
//        let day     = dataDict.value(forKey: "time_ago") as? String ?? ""
        
//        cell.lblNotification.text = message
//        cell.lblTimeNotifi.text   = day
        
        cell.lblNotification.sizeToFit()
        cell.lblNotification.layoutIfNeeded()
        cell.lblTimeNotifi.sizeToFit()
        cell.lblTimeNotifi.layoutIfNeeded()
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
}

//MARK:- Web Services Api Call
//MARK:-
extension NotificationVC{
    
    func notificationApiCall() {
        
        let passDict = [
//                        "token":token,
                        "user_id":userID
                        ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            webServicesConnection.requestPOSTURL("notification", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    print("THE Notification RESPONSE IS", responseJson)
                    
                    self.notificationListing_Array = responseJson["response listing"].arrayObject as? NSArray ?? []
                    
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                        Indicator.shared.hideProgressView()
                        let appDel = UIApplication.shared.delegate as! AppDelegate
                    _ = appDel.initLoginAtLogOut()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                        let message = responseJson["message"].stringValue
                        Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                }
                
            }, failure: { (error) in
                
                    Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
            
        else {
            
                Indicator.shared.hideProgressView()
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
                self.dismiss(animated: true, completion: nil)
        }
    }
    
}
