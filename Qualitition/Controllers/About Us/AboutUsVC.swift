//
//  AboutUsVC.swift
//  Qualitition
//
//  Created by Callsoft on 11/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController {
    
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        actInd.isHidden = true
        webView_SetUp()
        
    }
    
    @IBAction func btnBackToHome(_ sender: Any) {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.initHome()
    }
    
    
}

//MARK:- Custom Functions
//MARK:-
extension AboutUsVC {
    
    func webView_SetUp() {
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            webView.backgroundColor = UIColor.white
            webView.scrollView.showsVerticalScrollIndicator = false
            webView.scrollView.showsHorizontalScrollIndicator = false
            
            let url = URL(string: "http://mobulous.co.in/qualitian/services/about")
            
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
        }
        else {
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
        }
    }
}

//MARK:- WebView Delegate
//MARK:-
extension AboutUsVC: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        actInd.isHidden = false
        actInd.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        actInd.isHidden = true
        actInd.startAnimating()
    }
}
