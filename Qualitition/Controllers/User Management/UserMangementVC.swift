//
//  UserMangementVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class UserMangementVC: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()

    }

    
    @IBAction func btnBackToHome(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnAddUserLink(_ sender: Any) {
        
        let addUserVC = self.storyboard?.instantiateViewController(withIdentifier: "AddUser") as! AddUser
        
        self.navigationController?.pushViewController(addUserVC, animated: true)
    }
    
    
    @IBAction func btnPendingUserLink(_ sender: Any) {
        
        let pendingUserVC = self.storyboard?.instantiateViewController(withIdentifier: "PendingUserVC") as! PendingUserVC
        
        self.navigationController?.pushViewController(pendingUserVC, animated: true)
        
    }
    
    
    @IBAction func btnApprovedUserLink(_ sender: Any) {
        
        let approvedUserVC = self.storyboard?.instantiateViewController(withIdentifier: "ApprovedUserVC") as! ApprovedUserVC
        
        self.navigationController?.pushViewController(approvedUserVC, animated: true)
        
    }
    

}
