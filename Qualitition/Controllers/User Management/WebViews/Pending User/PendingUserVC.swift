//
//  PendingUserVC.swift
//  Qualitition
//
//  Created by Callsoft on 24/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class PendingUserVC: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var actIndiactor: UIActivityIndicatorView!
    
    let userID = UserDefaults.standard.value(forKey: "ResponseUserID") ?? ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

  //      landscapeRotation()
        
        actIndiactor.isHidden = true
        webView_Setup()

        print("THE PENDING SCREEN USER ID", userID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }

    @IBAction func btnBackToUserMana(_ sender: Any) {
        
   //     portraitRotation()
        self.navigationController?.popViewController(animated: true)
    }
    

}

//MARK:- Custom Functions
//MARK:-
extension PendingUserVC {
    
    func webView_Setup(){
        
    if InternetConnection.internetshared.isConnectedToNetwork() {
        
            webView.scrollView.showsHorizontalScrollIndicator = false
            webView.scrollView.showsVerticalScrollIndicator = false
      //      let url = URL (string: "http://mobulous.app/qualititian/pendinguser.html")
        let url = URL(string: "http://mobulous.co.in/qualitian/services/pendingUser1/\(userID)")
        
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
            
        }
        else {
        
        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
        }
    }
    
    
    func landscapeRotation() {
        
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    
    func portraitRotation(){
        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    
    private func shouldAutorotate() -> Bool {
        return true
    }
    
}

//MARK:- WebView Delegates
//MARK:-
extension PendingUserVC: UIWebViewDelegate{
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        actIndiactor.isHidden = false
        actIndiactor.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        actIndiactor.isHidden = true
        actIndiactor.stopAnimating()
        
    }
    
}
