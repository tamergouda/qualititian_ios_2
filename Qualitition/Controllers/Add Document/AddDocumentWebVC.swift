//
//  AddDocumentWebVC.swift
//  Qualitition
//
//  Created by Mac-As on 02/04/19.
//  Copyright © 2019 Callsoft. All rights reserved.
//

import UIKit

class AddDocumentWebVC: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    
    let userID = UserDefaults.standard.value(forKey: "ResponseUserID") ?? ""
    var template_ID = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        actInd.isHidden = true
        webView_Setup()
        
        print("THE PENDING SCREEN USER ID", userID)
        print("THE TEMPLATE ID", template_ID)
        
    }


   @IBAction func btnBackToHealth(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


//MARK:- Custom Functions
//MARK:-
extension AddDocumentWebVC {
    
    func webView_Setup() {
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            webView.backgroundColor = UIColor.white
            webView.scrollView.showsHorizontalScrollIndicator = false
            webView.scrollView.showsVerticalScrollIndicator = false
            
            let url = URL(string: "http://mobulous.co.in/qualitian/services/template_view/\(userID)/\(template_ID)")
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
            
        }
        else {
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
        }
        
    }
    
}

//MARK:- WebView Delegate
//MARK:-
extension AddDocumentWebVC: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        actInd.isHidden = false
        actInd.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        actInd.isHidden = true
        actInd.stopAnimating()
    }
}

