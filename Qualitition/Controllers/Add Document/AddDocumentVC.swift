//
//  AddDocumentVC.swift
//  Qualitition
//
//  Created by Callsoft on 19/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit
import MobileCoreServices
import DatePickerDialog

class AddDocumentVC: UIViewController {

    //MARK:- IBOutlets
    //MARK:-
    @IBOutlet weak var txtHospitalName: UITextField!
    @IBOutlet weak var lblAttachment: UILabel!
    @IBOutlet weak var txtDepartment: UITextField!
    @IBOutlet weak var txtNoOfPages: UITextField!
    @IBOutlet weak var txtPolicyTitle: UITextField!
    @IBOutlet weak var txtPolicyType: UITextField!
    @IBOutlet weak var txtPolicyCategory: UITextField!
    @IBOutlet weak var txtPolicyNumber: UITextField!
    @IBOutlet weak var txtEffectiveDate: UITextField!
    @IBOutlet weak var txtReviewDate: UITextField!
    @IBOutlet weak var txtReplaceNumber: UITextField!
    @IBOutlet weak var txtUpdate: UITextField!
    @IBOutlet weak var txtPurpose: UITextField!
    @IBOutlet weak var txtScope: UITextField!
    @IBOutlet weak var txtDefinition: UITextField!
    @IBOutlet weak var txtPolicy: UITextField!
    @IBOutlet weak var txtPocedure: UITextField!
    @IBOutlet weak var txtAttachments: UITextField!
    @IBOutlet weak var txtReference: UITextField!
    @IBOutlet weak var txtApprovals: UITextField!
    @IBOutlet weak var txtStandardNumber: UITextField!
    @IBOutlet weak var txtChapterNumber: UITextField!
    @IBOutlet weak var txtAccrediation: UITextField!
    @IBOutlet weak var txtPreparedByName: UITextField!
    @IBOutlet weak var txtReviewedByName: UITextField!
    
    
    
    
    @IBOutlet weak var btnLogoUpload: UIButton!
    @IBOutlet weak var imgLogo: UIImageView!
    
    
    
    
    //MARK:- Variables
    //MARK:-
    fileprivate let webServicesConnection = AlmofireWrapper()
    fileprivate var imagePicker = UIImagePickerController()
    fileprivate var imageData = NSData()     //To save the image as Data
    fileprivate let token = UserDefaults.standard.value(forKey: "responseToken")
    var defaultDate: Date!
    fileprivate var validation: Validation = Validation.validationManager() as! Validation
    fileprivate var documentName = "" // for Store the document Name
    
    //MARK:- View Life Cycle
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(token)
        
        imagePicker.allowsEditing = true
        restrictionForButton()
    }


    @IBAction func btnAttachment(_ sender: Any) {
        
        presentMenu()
    }
    

    @IBAction func btnBackToHealth(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEffectiveDatePick(_ sender: Any) {
        
        effectiveDatePicker()
    }
    
    @IBAction func btnReviewDatePick(_ sender: Any) {
        
        reviewDatePicker()
    }
    
    @IBAction func btnReplaceDate(_ sender: Any) {
        
        replaceDatePicker()
    }
    
    @IBAction func btnUpdatedate(_ sender: Any) {
        
        updateDatePicker()
    }
    
    @IBAction func btnSave(_ sender: Any) {
        
        validationSetup()
    }
    
    
    
    
}

//MARK:- Custom Functions
//MARK:-
extension AddDocumentVC{
    
    func restrictionForButton() {
        
        if (UserDefaults.standard.value(forKey: "UserRole")! as? String) == "Owner"{
            
            self.btnLogoUpload.addTarget(self, action: #selector(uploadLogo), for: .touchUpInside)
            
        }
        
        else if (UserDefaults.standard.value(forKey: "UserRole")! as? String) != "Owner"  {
            
            self.btnLogoUpload.addTarget(self, action: #selector(notAuthorisedForUploadImg), for: .touchUpInside)
        }
    }
    
    
    @objc func uploadLogo() {
        
        print("Show the Image Picker")
        openActionSheet()
    }
    
    @objc func notAuthorisedForUploadImg() {
        
        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "You are not authorized for upload a Logo", style: .error)
    }
    
    
    //for Effective Date button
    func effectiveDatePicker() {
        
        let date = Date()
        DatePickerDialog().show("Select the Effective Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: defaultDate ?? Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { (date) -> Void in
            
            
            if let dt = date
            {
                let formatter  = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                
                self.defaultDate = dt
                self.txtEffectiveDate.text = formatter.string(from: dt)
                
            }
        }
        
    }
        //for Review Date button
    func reviewDatePicker() {
        
        let date = Date()
        DatePickerDialog().show("Select the Review Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: defaultDate ?? Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { (date) -> Void in
            
            
            if let dt = date
            {
                let formatter  = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                
                self.defaultDate = dt
                self.txtReviewDate.text = formatter.string(from: dt)
                
            }
        }
        
    }
    
    //for Replace Date button
    func replaceDatePicker() {
        
        let date = Date()
        DatePickerDialog().show("Select the Review Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: defaultDate ?? Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { (date) -> Void in
            
            
            if let dt = date
            {
                let formatter  = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                
                self.defaultDate = dt
                self.txtReplaceNumber.text = formatter.string(from: dt)
                
            }
        }
        
    }
    
    //for Update Date button
    func updateDatePicker() {
        
        let date = Date()
        DatePickerDialog().show("Select the Review Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: defaultDate ?? Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { (date) -> Void in
            
            
            if let dt = date
            {
                let formatter  = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                
                self.defaultDate = dt
                self.txtUpdate.text = formatter.string(from: dt)
                
            }
        }
        
    }
    
        //TO Set the textfield empty after succ respon
    func emptyTheFieldAfterSuccessResponse() {
        
        txtHospitalName.text! = ""
        txtDepartment.text! = ""
        txtNoOfPages.text! = ""
        txtPolicyTitle.text! = ""
        txtPolicyType.text! = ""
        txtPolicyCategory.text! = ""
        txtPolicyNumber.text! = ""
        txtEffectiveDate.text! = ""
        txtReviewDate.text! = ""
        txtReplaceNumber.text! = ""
        txtUpdate.text! = ""
        txtPurpose.text! = ""
        txtScope.text! = ""
        txtDefinition.text! = ""
        txtPolicy.text! = ""
        txtPocedure.text! = ""
        txtAttachments.text! = ""
        txtReference.text! = ""
        txtApprovals.text! = ""
        txtStandardNumber.text! = ""
        txtChapterNumber.text! = ""
        txtAccrediation.text! = ""
        txtPreparedByName.text! = ""
        txtReviewedByName.text! = ""
        
        lblAttachment.text = "Attachment"
        
    }
    
    
    
}
//MARK:- Validations
//MARK:-
extension AddDocumentVC{

    func validationSetup()->Void{
        
        var message = ""
        
        if !validation.validateBlankField(txtHospitalName.text!){
            message = "Please enter your Hospital Name"
        }
        else if !validation.validateBlankField(txtDepartment.text!) {
            message = "Please enter your Department"
        }
            
        else if !validation.validateBlankField(txtNoOfPages.text!){
            message = "Please enter Number of Pages"
        }
        else if !validation.validateBlankField(txtPolicyTitle.text!){
            message = "Please enter Policy Title"
        }
        else if !validation.validateBlankField(txtPolicyType.text!){
            message = "Please enter Policy type"
        }
        else if !validation.validateBlankField(txtPolicyCategory.text!){
            message = "Please enter Policy Category"
        }
        else if !validation.validateBlankField(txtPolicyNumber.text!){
            message = "Please enter Policy Number"
        }
        else if !validation.validateBlankField(txtEffectiveDate.text!){
            message = "Please Select Effective Date"
        }
        else if !validation.validateBlankField(txtReviewDate.text!){
            message = "Please Select Review date"
        }
        else if !validation.validateBlankField(txtReplaceNumber.text!){
            message = "Please Select Replace date"
        }
        else if !validation.validateBlankField(txtUpdate.text!){
            message = "Please Select Update date"
        }
        else if !validation.validateBlankField(txtPurpose.text!){
            message = "Please enter Purpose"
        }
        else if !validation.validateBlankField(txtScope.text!){
            message = "Please enter Scope"
        }
        else if !validation.validateBlankField(txtDefinition.text!){
            message = "Please enter Definition"
        }
        else if !validation.validateBlankField(txtPolicy.text!){
            message = "Please enter Policy"
        }
        else if !validation.validateBlankField(txtPocedure.text!){
            message = "Please enter Procedure"
        }
        else if !validation.validateBlankField(txtAttachments.text!){
            message = "Please enter Attachments"
        }
        else if !validation.validateBlankField(txtReference.text!){
            message = "Please enter Reference"
        }
        else if !validation.validateBlankField(txtApprovals.text!){
            message = "Please enter Approvals"
        }
        else if !validation.validateBlankField(txtStandardNumber.text!){
            message = "Please enter Standard Number"
        }
        else if !validation.validateBlankField(txtChapterNumber.text!){
            message = "Please enter Chapter Number"
        }
        else if !validation.validateBlankField(txtAccrediation.text!){
            message = "Please enter Accrediation"
        }
        else if !validation.validateBlankField(txtPreparedByName.text!){
            message = "Please enter Prepared Name"
        }
        else if !validation.validateBlankField(txtReviewedByName.text!){
            message = "Please enter Reviewed Name"
        }
        
        if message != "" {
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: AlertStyle.error)
            
        }else{
            
            addDocumentApiCall()
            
        }
        
    }
}

//MARK:- Image Picker Delegate Functions
//MARK:-
extension AddDocumentVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func openActionSheet() {
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //Open Camera
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self .present(imagePicker, animated: true, completion: nil)
        }else {
            
            let alert = UIAlertController(title: ALERT_MESSAGE.ALERT_TITLE, message: "You don't have camera", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //Open Gallary
    func openGallery() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    //ImagePicker Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        imagePicker.dismiss(animated: true, completion: nil)
        
        let chosenImage = info[UIImagePickerControllerEditedImage] as? UIImage
        
        if chosenImage != nil {
            imageData = UIImageJPEGRepresentation(chosenImage!, 0.5) as! NSData
        }
        else {
            print("error")
            return
        }
        
        imgLogo.image = chosenImage
        //  imgUser.layer.cornerRadius = imgUser.frame.size.width/2
        imgLogo.clipsToBounds = true
        
         UserDefaults.standard.set(imageData, forKey: "imageData")
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- Document Picker Delegate
extension AddDocumentVC: UIDocumentMenuDelegate,UIDocumentPickerDelegate {
    
    func presentMenu() {
        
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeRTF), String(kUTTypePNG), String(kUTTypeJPEG), String(kUTTypeURL), String(kUTTypePlainText),String(kUTTypeText),String(kUTTypeSpreadsheet), String(kUTTypePresentation),String("com.microsoft.word.doc"), String("com.microsoft.excel.xls"), String("com.microsoft.powerpoint.​ppt")], in: .import)
            
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        
        self.present(importMenu, animated: true, completion: nil)
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        

        if controller.documentPickerMode == UIDocumentPickerMode.import {
            
            let path = URL(fileURLWithPath: url.path)
            
            print("********")
            
            let indexFileName = (path.pathComponents.last)!
            
            do {
                
                let indexData = try NSData(contentsOf: path)
                //    print("THE INDEX DATA IS", indexData) converted to NSDATA
                UserDefaults.standard.set(indexData, forKey: "documentData")

            }
                
            catch {
                
                print("Unable to load data: (error)")
            }
            
            
            print("@@@@@@@@@@@")
            
            print("THE INDEX FILE NAME", indexFileName)
            
            self.lblAttachment.text = indexFileName
            self.documentName = indexFileName
            print("THE DOCUMENT NAME IS", self.documentName)
            
            var mimeString = indexFileName.components(separatedBy: ".")
            
            _ = mimeString[0]
            
            let index2 = mimeString[1]
            
            print("THE MIME STRING", mimeString)
            
            print("THE INDEX 2 of MIME", index2)
            
            
            let fileManager = FileManager.default
            
            let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(indexFileName)
            
            print("##########")
            
            let url1 = URL(string: indexFileName)
            
            print("THE URL1 is", url1 as Any)
            
            if fileManager.fileExists(atPath: paths) {
                
                print("THE URL OF THE PATHS", url)
            }
                
            else {
                
                print("NO URL")
            }
            
            print("@@@@@@@@@@@@@@@")
            
            
        }
        
        
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    
}

//MARK:- Web Services Api Call
//MARK:-
extension AddDocumentVC{
    
    func addDocumentApiCall(){
        
        let passDict = [
            "token": token,
            "hospital_name":txtHospitalName.text!,
            "department":txtDepartment.text!,
            "no_pages": txtNoOfPages.text!,
            "policy_title": txtPolicyTitle.text!,
            "policy_category": txtPolicyCategory.text!,
            "policy_number": txtPolicyNumber.text!,
            "policy_type": txtPolicyType.text!,
            "effective_date":txtEffectiveDate.text!,
            "review_date":txtReviewDate.text!,
            "replace_date":txtReplaceNumber.text!,
            "update_date": txtUpdate.text!,
            "purpose": txtPurpose.text!,
            "scope": txtScope.text!,
            "definations": txtDefinition.text!,
            "policy":txtPolicy.text!,
            "procedures": txtPocedure.text!,
            "attechments": txtAttachments.text!,
            "reference": txtReference.text!,
            "approvle": txtApprovals.text!,
            "standard_no": txtStandardNumber.text!,
            "chapter_no": txtChapterNumber.text!,
            "accrediation": txtAccrediation.text!,
            "prepared_by": txtPreparedByName.text!,
            "reviewed_by": txtReviewedByName.text!
            //            "hospital_logo": //data for attachment,
            //            "file":  //data for attachment,
            ] as [String : Any]
        
        if (UserDefaults.standard.value(forKey: "imageData") != nil) && (UserDefaults.standard.value(forKey: "documentData") != nil) {
            
            let imagedata = UserDefaults.standard.value(forKey: "imageData") as! NSData
            let documentdata = UserDefaults.standard.value(forKey: "documentData") as! NSData
            
            if InternetConnection.internetshared.isConnectedToNetwork() {
                Indicator.shared.showProgressView(view)
                
                webServicesConnection.requWithFilewith2Data(imageData: imagedata, audioData: documentdata, fileName1: "image.jpg", fileName2: documentName, imageparam1: "hospital_logo", imageparam2: "file", urlString: "addDocument", parameters: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                    
                    if responseJson["status"].stringValue == "Success" {
                        
                        Indicator.shared.hideProgressView()
                        print("THE Add Document Response is", responseJson)
                        
                        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Document Successfully Uploaded", style: .success)
                        
                        UserDefaults.standard.removeObject(forKey: "imageData")
                        UserDefaults.standard.removeObject(forKey: "documentData")
                        
                        self.emptyTheFieldAfterSuccessResponse()
                    }
                        
                    else if responseJson["message"].stringValue == "Invalid User." {
                        
                        Indicator.shared.hideProgressView()
                        let appDel = UIApplication.shared.delegate as! AppDelegate
                        appDel.initLoginAtLogOut()
                        
                        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                    }
                        
                    else {
                        
                        let message = responseJson["message"].stringValue
                        Indicator.shared.hideProgressView()
                        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                        
                    }
                    
                }, failure: { (error) in
                    
                    Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                    
                })
            }
                
            else {
                
                Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
                self.dismiss(animated: true, completion: nil)
            }
            
            
        }
        
       else if (UserDefaults.standard.value(forKey: "imageData") != nil) {
            let imagedata = UserDefaults.standard.value(forKey: "imageData") as! NSData
            
            if InternetConnection.internetshared.isConnectedToNetwork() {
                Indicator.shared.showProgressView(view)
                
                webServicesConnection.requWithFile(imageData: imagedata, fileName: "image.jpg", imageparam: "hospital_logo", urlString: "addDocument", parameters: passDict as [String : AnyObject], headers: nil, success: { (responseJson) in
                    
                    if responseJson["status"].stringValue == "Success" {
                        
                        Indicator.shared.hideProgressView()
                        print("THE Add Document Response is", responseJson)
                        
                        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Document Successfully Uploaded", style: .success)
                        
                        UserDefaults.standard.removeObject(forKey: "imageData")
                        self.emptyTheFieldAfterSuccessResponse()
                    }
                        
                    else if responseJson["message"].stringValue == "Invalid User." {
                        
                        Indicator.shared.hideProgressView()
                        let appDel = UIApplication.shared.delegate as! AppDelegate
                        appDel.initLoginAtLogOut()
                        
                        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                    }
                        
                    else {
                        
                        let message = responseJson["message"].stringValue
                        Indicator.shared.hideProgressView()
                        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                        
                    }
                    
                    
                }, failure: { (error) in
                    
                    Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                    
                    
                })
            }
            
            else {
                
                Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
                self.dismiss(animated: true, completion: nil)
                
            }
            
        }
            
        else if (UserDefaults.standard.value(forKey: "documentData") != nil) {
            
            let documentdata = UserDefaults.standard.value(forKey: "documentData") as! NSData
            
            if InternetConnection.internetshared.isConnectedToNetwork() {
                Indicator.shared.showProgressView(view)
                
                webServicesConnection.requWithAudioFile(audioData: documentdata, fileName: documentName, imageparam: "file", urlString: "addDocument", parameters: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                    
                    if responseJson["status"].stringValue == "Success" {
                        
                        Indicator.shared.hideProgressView()
                        print("THE Add Document Response is", responseJson)
                        
                        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Document Successfully Uploaded", style: .success)
                        
                        UserDefaults.standard.removeObject(forKey: "documentData")
                        self.emptyTheFieldAfterSuccessResponse()
                    }
                        
                    else if responseJson["message"].stringValue == "Invalid User." {
                        
                        Indicator.shared.hideProgressView()
                        let appDel = UIApplication.shared.delegate as! AppDelegate
                        appDel.initLoginAtLogOut()
                        
                        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                    }
                        
                    else {
                        
                        let message = responseJson["message"].stringValue
                        Indicator.shared.hideProgressView()
                        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                        
                    }
                    
                }, failure: { (error) in
                    
                    Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                })
            }
            
            else {
                
                Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
                self.dismiss(animated: true, completion: nil)
                
            }
            
        }
            
        
        else {
            
            if InternetConnection.internetshared.isConnectedToNetwork() {
                
                Indicator.shared.showProgressView(view)
                webServicesConnection.requestPOSTURL("addDocument", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                    
                    if responseJson["status"].stringValue == "Success" {
                        
                        Indicator.shared.hideProgressView()
                        
                        print("THE Add Document VC RESPONSE IS", responseJson)
                        self.emptyTheFieldAfterSuccessResponse()
                    }
                        
                    else if responseJson["message"].stringValue == "Invalid User." {
                        
                        Indicator.shared.hideProgressView()
                        let appDel = UIApplication.shared.delegate as! AppDelegate
                        appDel.initLoginAtLogOut()
                        
                        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                    }
                        
                    else {
                        
                        let message = responseJson["message"].stringValue
                        
                        Indicator.shared.hideProgressView()
                        
                        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                    }
                    
                }, failure: { (error) in
                    
                        Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                })
            }
                
            else {
                
                    Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
                    self.dismiss(animated: true, completion: nil)
            }
            
        }
        
    }  //Function Close

    
    
} //Extention Close
