//
//  SignUp_Type_VC.swift
//  Qualitition
//
//  Created by Callsoft on 11/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class SignUp_Type_VC: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()

    }

    
    @IBAction func btnGuest(_ sender: Any) {
        
        let guestSignupVC = self.storyboard?.instantiateViewController(withIdentifier: "GuestSignup") as! GuestSignup
        
        UserDefaults.standard.set(51, forKey: "guestUser")
        
        self.navigationController?.pushViewController(guestSignupVC, animated: true)
        
    }
    
    @IBAction func btnClient(_ sender: Any) {
        
        let corporateSignupVC = self.storyboard?.instantiateViewController(withIdentifier: "CorporateSignupVC") as! CorporateSignupVC
        
        UserDefaults.standard.set(52, forKey: "clientUser")
        
        self.navigationController?.pushViewController(corporateSignupVC, animated: true)
        
        
    }
    

    
    
}
