//
//  DocModuleVC.swift
//  Qualitition
//
//  Created by Callsoft on 13/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class DocModuleVC: UIViewController {


    @IBOutlet weak var tableView: UITableView!
    
    var documentTypeListing = NSArray()
    fileprivate let webServicesConnection = AlmofireWrapper()

    override func viewDidLoad() {
        super.viewDidLoad()

        DocumentModuleApiCall()
        self.tableView.addSubview(self.refreshControl)
    }

    @IBAction func btnBackToHome(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnGoToDocManag(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    
    
}

//MARK:- Custom Functions
//MARK:-
extension DocModuleVC{
    
        //INBUILD Refresh Control.
    var refreshControl: UIRefreshControl {
        
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action: #selector(DocModuleVC.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor(red: 59.0/255, green: 208.0/255, blue: 214.0/255, alpha: 1.0)
        
        return refreshControl
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        //Call the Module API here
        DocumentModuleApiCall()
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
}

//MARK:- TableView Functions
//MARK:-
extension DocModuleVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return documentTypeListing.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: DocModuleCell = tableView.dequeueReusableCell(withIdentifier: "DocModuleCell", for: indexPath) as! DocModuleCell
        
        let dataDict = documentTypeListing.object(at: indexPath.row) as? NSDictionary ?? [:]
        let templateName = dataDict.value(forKey: "template_type") as? String ?? ""
        
        cell.lblDocumentName.text = templateName
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let docVC = self.storyboard?.instantiateViewController(withIdentifier: "HealthVC") as! HealthVC
        let dataDict     = documentTypeListing.object(at: indexPath.row) as? NSDictionary ?? [:]
        let templateName = dataDict.value(forKey: "template_type") as? String ?? ""
        let templateID   = dataDict.value(forKey: "id") as? Int ?? 0
        
        docVC.headerTxt  = templateName
        docVC.templateID = templateID
        self.navigationController?.pushViewController(docVC, animated: true)
        
    }
}

//MARK:- Web Services Api Call
//MARK:-
extension DocModuleVC{
    
    func DocumentModuleApiCall(){
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            Indicator.shared.showProgressView(view)
            
            webServicesConnection.requestGETURL("TemplateType", success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    Indicator.shared.hideProgressView()
                    
                    self.documentTypeListing = responseJson["templateType"].arrayObject as? NSArray ?? []
                    
                    self.tableView.reloadData()
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                        Indicator.shared.hideProgressView()
                        let appDel = UIApplication.shared.delegate as! AppDelegate
                        _ = appDel.initLoginAtLogOut()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                        let message = responseJson["message"].stringValue
                        Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                }
                
            }, failure: { (error) in
                    print("THE ERROR IS", error.localizedDescription)
                
                    Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
            
        else {
            
            Indicator.shared.hideProgressView()
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
