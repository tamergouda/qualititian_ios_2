//
//  AnalyticsVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class AnalyticsVC: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func btnBackToHome(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnUploadedDoc(_ sender: Any) {
        
        let uploadVC = self.storyboard?.instantiateViewController(withIdentifier: "UploadedDocumentVC") as! UploadedDocumentVC
        
        self.navigationController?.pushViewController(uploadVC, animated: true)
        
    }
    
    @IBAction func btnDownloadDoc(_ sender: Any) {
        
        let downloadVC = self.storyboard?.instantiateViewController(withIdentifier: "DownloadedDocVC") as! DownloadedDocVC
        
        self.navigationController?.pushViewController(downloadVC, animated: true)
    }
    
    
    @IBAction func btnPublishedDoc(_ sender: Any) {
        
        let publishVC = self.storyboard?.instantiateViewController(withIdentifier: "PublishDocVC") as! PublishDocVC
        
        self.navigationController?.pushViewController(publishVC, animated: true)
        
    }
    
    @IBAction func btnPendingDoc(_ sender: Any) {
        
        let pendingDocuVC = self.storyboard?.instantiateViewController(withIdentifier: "PendingDocVC") as! PendingDocVC
        
        self.navigationController?.pushViewController(pendingDocuVC, animated: true)
    }
    
    
    @IBAction func btnTimeConsumed(_ sender: Any) {
    }
    
    @IBAction func btnTimeOnDoc(_ sender: Any) {
    }
    
    @IBAction func btnTimeOnApp(_ sender: Any) {
    }
    
}
