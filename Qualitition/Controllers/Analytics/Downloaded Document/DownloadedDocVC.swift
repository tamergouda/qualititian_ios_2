//
//  DownloadedDocVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/10/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class DownloadedDocVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var webServicesConnection = AlmofireWrapper()
    fileprivate let token = UserDefaults.standard.value(forKey: "responseToken")
    fileprivate var downloadListingArray = NSArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        downloadDocApiCall()
        self.tableView.addSubview(self.refreshControl)
        
    }

   
    @IBAction func btnBackToAnalytics(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    

}

//MARK:- Table View Delegate & DataSource Functions
//MARK:-
extension DownloadedDocVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return downloadListingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DownloadedDocCell", for: indexPath) as! DownloadedDocCell
        
        let dataDict = self.downloadListingArray.object(at: indexPath.row) as? NSDictionary ?? [:]
        
        cell.lblDocument.text = dataDict.value(forKey: "document_title") as? String ?? ""
        
        let imageURL = dataDict.value(forKey: "hospital_logo") as? String ?? ""
        cell.imgDocument.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "Default_logo"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 145
    }
}

//MARK:- Custom Functions
//MARK:-
extension DownloadedDocVC{
    
    //INBUILD Refresh Control.
    var refreshControl: UIRefreshControl {
        
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action: #selector(DownloadedDocVC.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor(red: 59.0/255, green: 208.0/255, blue: 214.0/255, alpha: 1.0)
        
        return refreshControl
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        //Call the Module API here
        downloadDocApiCall()
        
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
}

//MARK:- Web Services API Call
//MARK:-
extension DownloadedDocVC {
    
    func downloadDocApiCall() {
        
        let passDict =  [
                        "token":token
                        ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            webServicesConnection.requestPOSTURL("dwonloadListing", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    Indicator.shared.hideProgressView()
                    print("THE Download Listing RESPONSE IS", responseJson)
                    
                    self.downloadListingArray = responseJson["dwonloadListing"].arrayObject as? NSArray ?? []
                    
                    self.tableView.reloadData()
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                    Indicator.shared.hideProgressView()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                    let message = responseJson["message"].stringValue
                    
                    Indicator.shared.hideProgressView()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                }
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
            
        else {
            
            Indicator.shared.hideProgressView()
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
