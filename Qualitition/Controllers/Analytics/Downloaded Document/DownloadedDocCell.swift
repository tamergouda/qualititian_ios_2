//
//  DownloadedDocCell.swift
//  Qualitition
//
//  Created by Callsoft on 10/10/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class DownloadedDocCell: UITableViewCell {

    @IBOutlet weak var imgDocument: UIImageView!
    @IBOutlet weak var lblDocument: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
