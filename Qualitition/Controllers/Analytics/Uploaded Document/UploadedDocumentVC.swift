//
//  UploadedDocumentVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/10/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit
import SDWebImage

class UploadedDocumentVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    fileprivate var webServicesConnection = AlmofireWrapper()
    fileprivate let token = UserDefaults.standard.value(forKey: "responseToken")
    fileprivate var documentArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        uploadDocApiCall()
        self.tableView.addSubview(self.refreshControl)

    }
    

    @IBAction func btnBackToAnalytics(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    

}

//MARK:- Custom Functions
//MARK:-
extension UploadedDocumentVC {
    
        var refreshControl: UIRefreshControl {
            
            let refreshControl = UIRefreshControl()
            
            refreshControl.addTarget(self, action: #selector(UploadedDocumentVC.handleRefresh(_:)), for: .valueChanged)
            
            refreshControl.tintColor = UIColor(red: 59/255, green: 208/255, blue: 214/255, alpha: 1.0)
            
            return refreshControl
        }
        
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        uploadDocApiCall()
        
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    
    
}

//MARK:- Table View Delegate & DataSource Functions
//MARK:-
extension UploadedDocumentVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return documentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UploadedDocTableCell", for: indexPath) as! UploadedDocTableCell
        
        if documentArray.count >= 0 {
            
            let dataDict = documentArray.object(at: indexPath.row) as? NSDictionary ?? [:]
            

            cell.lblDocument.text = dataDict.value(forKey: "document_title") as? String ?? ""
            let responseImgUrl = dataDict.value(forKey: "hospital_logo") as? String ?? ""
            cell.imgDocument.sd_setImage(with: URL(string: responseImgUrl), placeholderImage: #imageLiteral(resourceName: "Default_logo"))
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 145
    }
    
}

//MARK:- Web Services API Call
//MARK:-
extension UploadedDocumentVC {
    
    func uploadDocApiCall() {
        
        let passDict =  [
            "token":token,
            ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            webServicesConnection.requestPOSTURL("documentListing", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    

                    self.documentArray = responseJson["documentListing"].arrayObject as? NSArray ?? []
                    print("THE DOCUMENT ARRAY", self.documentArray)
                    self.tableView.reloadData() // must do that otherwise it showing the nil
                    
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                    Indicator.shared.hideProgressView()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                    let message = responseJson["message"].stringValue
                    Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                }
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
            
        else {
            
            Indicator.shared.hideProgressView()
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
