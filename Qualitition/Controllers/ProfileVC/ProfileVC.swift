//
//  ProfileVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController, UITextFieldDelegate {
    
    //MARK:- IBOutlets
    //MARK:-
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var txtCoorporateName: UITextField!
    @IBOutlet weak var txtDepartment: UITextField!
    @IBOutlet weak var txtDesignation: UITextField!
    @IBOutlet weak var btnSaveUpdates: UIButton!
    @IBOutlet weak var btnPayment: UIButton!
    
    
    //The change password fields.
    @IBOutlet weak var changePassView: UIView!
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    //change password View.
    @IBOutlet weak var oldPassView: UIView!
    @IBOutlet weak var newPassView: UIView!
    @IBOutlet weak var confirmPassView: UIView!
    @IBOutlet var heightContstraint: NSLayoutConstraint!
    
    //The Submit Button of change password.
    @IBOutlet weak var btnSubmitOut: UIButton!
    
    //MARK:- Variables
    //MARK:-
    fileprivate var webServicesConnection = AlmofireWrapper()
    fileprivate var validation:Validation = Validation.validationManager() as! Validation
    
    fileprivate let token = UserDefaults.standard.value(forKey: "responseToken")
    
    
    //MARK:- View Life Cycle
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetUp()
        viewProfileApi()
        changePassView.isHidden = true
    }
    
    
    //MARK:- IBAction Button
    //MARK:-
    @IBAction func btnBackToHome(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNotification(_ sender: Any) {
        let notificVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notificVC, animated: true)
    }
    
    @IBAction func btnEditProfile(_ sender: Any) {
        
        btnSaveUpdates.isHidden = false
        self.txtName.becomeFirstResponder()
        
        txtName.isUserInteractionEnabled = true
        //       txtEmail.isUserInteractionEnabled = true  //Because No edit Allow on email & No.
        //       txtNumber.isUserInteractionEnabled = true
        txtCoorporateName.isUserInteractionEnabled = true
        txtDepartment.isUserInteractionEnabled = true
        txtDesignation.isUserInteractionEnabled = true
        
    }
    
    @IBAction func btnSaveProfile(_ sender: Any) {
        editProfileApiCall()
    }
    
    
    @IBAction func btnChangePass(_ sender: Any) {
        
        self.changePassView.isHidden = false
        
        UIView.animate(withDuration: 0.4) {
            
            self.oldPassView.isHidden = false
            self.newPassView.isHidden = false
            self.confirmPassView.isHidden = false
            self.txtOldPassword.isHidden = false
            self.txtNewPassword.isHidden = false
            self.txtConfirmPassword.isHidden = false
            self.btnSubmitOut.isHidden = false
            self.heightContstraint.constant = 260
            //            self.oldPassViewConstraint.constant = 50
            //            self.newPassViewConstraint.constant = 50
            //            self.confirmPassViewConstraint.constant = 50
            self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func btnClose(_ sender: Any) {
        hideChangePassView()
    }
    
    @IBAction func btnChangePassword(_ sender: Any) {
        validationSetup()
        
    }
    
    
}

//MARK:- Custom Functions
//MARK:-
extension ProfileVC {
    
    //MARK:- Default Animation here, Do this for the Payment UIView... Just call the Function on button Tab.
    //    func animateView(){
    //
    //        self.paymentView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
    //        UIView.animate(withDuration: 1.0,
    //                       delay: 0,
    //                       usingSpringWithDamping: 0.25,
    //                       initialSpringVelocity: 8.00,
    //                       options: .allowUserInteraction,
    //                       animations: {
    //                        self.paymentView.transform = CGAffineTransform.identity
    //        })
    //
    //    }
    
    func initialSetUp() {
        
        btnSaveUpdates.isHidden = true
        
        txtName.isUserInteractionEnabled = false
        txtEmail.isUserInteractionEnabled = false
        txtNumber.isUserInteractionEnabled = false
        txtCoorporateName.isUserInteractionEnabled = false
        txtDepartment.isUserInteractionEnabled = false
        txtDesignation.isUserInteractionEnabled = false
        
        txtName.delegate = self  //for alphabets only
    }
    
    func hideChangePassView() {
        
        UIView.animate(withDuration: 0.4) {
            
            self.oldPassView.isHidden = true
            self.newPassView.isHidden = true
            self.confirmPassView.isHidden = true
            self.txtOldPassword.isHidden = true
            self.txtNewPassword.isHidden = true
            self.txtConfirmPassword.isHidden = true
            self.btnSubmitOut.isHidden = true
            self.heightContstraint.constant = 0
            self.view.layoutIfNeeded()
        }
        
    }
    
    //For alphabets Only
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let texts = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ")
        
        if textField == txtName
        {
            guard let text = txtName.text else { return true }
            
            let newLength = text.characters.count + string.characters.count
                - range.length
            return newLength <= 15 && (txtName.text! + string).rangeOfCharacter(from: texts.inverted) == nil
        }
        
        return true
    }
    
    
    //MARK:- Validations
    //MARK:-
    func validationSetup()->Void{
        
        
        var message = ""
        
        
        if !validation.validateBlankField(txtOldPassword.text!){
            message = "Please enter Old Password"
        }
            
        else if !validation.validateBlankField(txtNewPassword.text!) {
            message = "Please enter New Password"
        }
            
        else if txtNewPassword.text! != txtConfirmPassword.text!{
            message = "New Password and Confirm Password must be same"
        }
        
        
        if message != "" {
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: AlertStyle.error)
            
        }else{

            changePassAPI()
        }
        
        
    }
    
    
}

//MARK:- Web Services API Call
//MARK:-
extension ProfileVC {
    
    func changePassAPI() {
        
        let passDict = [
            "oldpassword": txtOldPassword.text!,
            "newpassword": txtNewPassword.text!,
            "token": token
            ] as [String : Any]
        
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            
            webServicesConnection.requestPOSTURL("changePassword", params: passDict as [String : AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    
                    self.hideChangePassView()
                    print("Your Password is changed successfully.")
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Password Change Successfully", style: .success)
                    
                    self.txtOldPassword.text = ""
                    self.txtNewPassword.text = ""
                    self.txtConfirmPassword.text = ""
                    
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                    Indicator.shared.hideProgressView()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                    let message = responseJson["message"].stringValue
                    
                    Indicator.shared.hideProgressView()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                    
                }
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                
            })
        }
            
        else {
            
            Indicator.shared.hideProgressView()
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            
            self.dismiss(animated: true, completion: nil)
            
        }
        
    }
    
    
    func editProfileApiCall() {
        
        let passDict = [
            "first_name": txtName.text!,
            "email": txtEmail.text!,
            "mobile": txtNumber.text!,
            "type_corprate": txtCoorporateName.text!,
            "department_name": txtDepartment.text!,
            "designation": txtDesignation.text!,
            "token": token,
            ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            
            webServicesConnection.requestPOSTURL("editProfile", params: passDict as [String : AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    print("profile Edited", responseJson)
                    
                    let userData = responseJson["response"].dictionaryObject as? NSDictionary ?? [:]
                    
                    let name = userData.value(forKey: "first_name") as? String ?? ""
                    //                    UserDefaults.standard.set(name, forKey: "UserName")  // overrite name from login. Doing this in ViewProfileAPI.
                    
                    print(name)
                    self.initialSetUp()
                    self.viewProfileApi()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Your Profile updated  successfully.", style: .success)
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                    Indicator.shared.hideProgressView()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                    let message = responseJson["message"].stringValue
                    
                    Indicator.shared.hideProgressView()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                    
                }
                
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                
            })
        }
            
        else {
            
            Indicator.shared.hideProgressView()
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            
            self.dismiss(animated: true, completion: nil)
            
        }
    }
    
    func viewProfileApi(){
        
        let passDict = [
            "token": token
            ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            
            webServicesConnection.requestPOSTURL("viewProfile", params: passDict as [String : AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    print("User profile", responseJson)
                    
                    let userData = responseJson["viewProfile"].dictionaryObject as? NSDictionary ?? [:]
                    //   print("THE DATA", userData)
                    
                    let name = userData.value(forKey: "first_name") as? String ?? ""
                    UserDefaults.standard.set(name, forKey: "UserName")  // overrite name from login
                    
                    self.txtName.text = name
                    self.txtEmail.text = userData.value(forKey: "email") as? String ?? ""
                    self.txtNumber.text = userData.value(forKey: "mobile") as? String ?? ""
                    self.txtCoorporateName.text = userData.value(forKey: "type_corprate") as? String ?? ""
                    self.txtDepartment.text = userData.value(forKey: "department_name") as? String ?? ""
                    self.txtDesignation.text = userData.value(forKey: "designation") as? String ?? ""
                    
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                    Indicator.shared.hideProgressView()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                    let message = responseJson["message"].stringValue
                    
                    Indicator.shared.hideProgressView()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                    
                }
                
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                
            })
        }
            
        else {
            
            Indicator.shared.hideProgressView()
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            
            self.dismiss(animated: true, completion: nil)
            
        }
        
        
    }
    
    
}
