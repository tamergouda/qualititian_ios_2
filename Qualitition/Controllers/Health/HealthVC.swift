//
//  HealthVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class HealthVC: UIViewController{
    
    
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblHeaderText: UILabel!
    
    @IBOutlet weak var btnSort: UIButton!
    @IBOutlet weak var sortView: UIView!
    
    
    var dummyArrayImage = [#imageLiteral(resourceName: "document_doc"),#imageLiteral(resourceName: "document_pdf"),#imageLiteral(resourceName: "document_doc"),#imageLiteral(resourceName: "document_pdf")]
    
    var headerTxt:String = ""
    var templateID: Int? //Coming from DocModuleVC Response
    var templateNameArray = NSArray()
    
    //******* SearchBar
    var searchActive = false
    var filtered : NSMutableArray = NSMutableArray()
    
    fileprivate let webServicesConnection = AlmofireWrapper()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblHeaderText.text = headerTxt
        
        sortView.isHidden = true
        btnSort.addTarget(self, action: #selector(btnSortTabbed), for: .touchUpInside)
        
        templateListingApi()
        self.collectionView.addSubview(self.refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        search.text = ""
        searchActive = false
        collectionView.reloadData()
    }
    
    @IBAction func btnBackToDocModule(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //Header Sort Button
    @objc func btnSortTabbed() {
        
        if btnSort.isSelected == true {
            sortView.isHidden = false
            btnSort.isSelected = false
        }else {
            sortView.isHidden = true
            btnSort.isSelected = true
        }
    }
    
    
    
}


//MARK:- Custom Functions
//MARK:-
extension HealthVC: UISearchBarDelegate{
    
    // When button "Search" pressed on search Bar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.search.endEditing(true)
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        if searchBar.text == "" {
            searchActive = false
        }
            
        else {
            searchActive = true
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        print("THE ENTER TEXT", searchText)
        filtered.removeAllObjects()
        
        if searchBar.text!.isEmpty {
            searchActive = false
        }
            
        else {
            
            searchActive = true
            
            if templateNameArray.count >= 1 {
                
                for index in 0...templateNameArray.count-1 {
                    
                    let dicResponse = templateNameArray.object(at: index) as? NSDictionary ?? [:]
                    let documentName = dicResponse.object(forKey: "template_name") as? String ?? ""
                    
                    if (documentName.lowercased().range(of: searchText.lowercased()) != nil) {
                        
                        filtered.add(dicResponse)
                    }
                }
            }
        }
        
        collectionView.reloadData()
    }
    
    
    var refreshControl: UIRefreshControl {
        
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action: #selector(HealthVC.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor(red: 59/255, green: 208/255, blue: 214/255, alpha: 1.0)
        
        return refreshControl
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        //Call the Health API here
        
        self.collectionView.reloadData()
        refreshControl.endRefreshing()
    }
}


//MARK:- CollectionView Functions
//MARK:-
extension HealthVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return templateNameArray.count
        
        if searchActive == true {
            
            return filtered.count
        }
            
        else {
            return templateNameArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: HealthViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HealthViewCell", for: indexPath) as! HealthViewCell
        
        
        if searchActive == true {
            
            let dataDict     = filtered.object(at: indexPath.row) as? NSDictionary ?? [:]
            let templateName = dataDict.value(forKey: "template_name") as? String ?? ""
            let imageURL     = dataDict.value(forKey: "file") as? String ?? ""
            
            cell.lblTemplate.text = templateName
            cell.imgHealthDocument.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "Default_logo"))
        }
        else {
            
            let dataDict     = templateNameArray.object(at: indexPath.row) as? NSDictionary ?? [:]
            let templateName = dataDict.value(forKey: "template_name") as? String ?? ""
            let imageURL     = dataDict.value(forKey: "file") as? String ?? ""
            cell.lblTemplate.text = templateName
            cell.imgHealthDocument.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "Default_logo"))
            
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellsAcross: CGFloat = 2
        let spaceBetweenCells: CGFloat = 20
        let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
        
        return CGSize(width: dim, height: dim)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if searchActive == true {
            
            let dataDict   = filtered.object(at: indexPath.row) as? NSDictionary ?? [:]
            let templateID = dataDict.value(forKey: "template_id") as? Int ?? 0
            
            
            let addDocVC: AddDocumentWebVC = self.storyboard?.instantiateViewController(withIdentifier: "AddDocumentWebVC") as! AddDocumentWebVC
            
            addDocVC.template_ID = templateID
            self.navigationController?.pushViewController(addDocVC, animated: true)
            
        }
            
        else {
            
            let dataDict     = templateNameArray.object(at: indexPath.row) as? NSDictionary ?? [:]
            let templateID = dataDict.value(forKey: "template_id") as? Int ?? 0
            
            
            let addDocVC: AddDocumentWebVC = self.storyboard?.instantiateViewController(withIdentifier: "AddDocumentWebVC") as! AddDocumentWebVC
            addDocVC.template_ID = templateID
            self.navigationController?.pushViewController(addDocVC, animated: true)
            
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(withDuration: 0.8) {
            cell.alpha = 1
        }
        
    }
    
}

//MARK:- Web Services API Call
//MARK:-
extension HealthVC {
    
    func templateListingApi() {
        
        let passDict = [
                        "template_type":self.templateID ?? 0,
                        ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            webServicesConnection.requestPOSTURL("TemplateListing", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    Indicator.shared.hideProgressView()
                    
                    let documentDummyArray = self.templateNameArray.mutableCopy() as? NSMutableArray ?? []
                    documentDummyArray.removeAllObjects()
                    self.templateNameArray = documentDummyArray.mutableCopy() as? NSArray ?? []
                    
                    
                    print("THE Template VC RESPONSE IS", responseJson)
                    
                    self.templateNameArray = responseJson["templateListing"].arrayObject as? NSArray ?? []
                    
                    self.collectionView.reloadData()
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                        Indicator.shared.hideProgressView()
                        let appDel = UIApplication.shared.delegate as! AppDelegate
                    _ = appDel.initLoginAtLogOut()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                        let message = responseJson["message"].stringValue
                        Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                }
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
            
        else {
            
            Indicator.shared.hideProgressView()
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
