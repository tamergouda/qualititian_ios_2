//
//  ForgotPassVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class ForgotPassVC: UIViewController {


    @IBOutlet weak var txtEmailOrPhone: UITextField!
    
    
   fileprivate var validation: Validation = Validation.validationManager() as! Validation
   fileprivate var webServicesConnection = AlmofireWrapper()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }


    @IBAction func btnNext(_ sender: Any) {
        validationSetup()
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnBackToLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

}

//MARK:- Custom Functions
//MARK:-
extension ForgotPassVC{
    

    func validationSetup()->Void{
        
        var message = ""
        
        if !validation.validateBlankField(txtEmailOrPhone.text!){
            message = "Please enter your E-mail ID / Phone Number"
            
        }
        else if !validation.validateEmail(txtEmailOrPhone.text!) && !validation.validatePhoneNumber(txtEmailOrPhone.text!){
            
            message = "Please enter valid E-mail ID / Phone Number"
        }
        
        if message != "" {
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: AlertStyle.error)
            
        }else{
            
            forgotPasswordApiCall()
        }
        
        
    }
    
    
}

//MARK:- Web Services API Call
//MARK:-
extension ForgotPassVC {
    
    func forgotPasswordApiCall() {
        
        let passDict = [
            "email": txtEmailOrPhone.text! //Need to have the phone number also
            ] as [String : Any]
     
        UserDefaults.standard.set(txtEmailOrPhone.text, forKey: "otpRecoverEmail")
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            
            webServicesConnection.requestPOSTURL("forgotPassword", params: passDict as [String : AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    
                     print("THE FORGOT PASSWORD RESPONSE",responseJson)
                    
                    let dataDict = responseJson["forgotPassword"].dictionaryObject as? NSDictionary ?? [:]
                    
                    print("THE DICTIONART", dataDict)
                    
                    let resetVC: ResetOTPVC = self.storyboard?.instantiateViewController(withIdentifier: "ResetOTPVC") as! ResetOTPVC
                    
                    resetVC.OTPVALUE = dataDict.value(forKey: "code") as? String ?? ""
                    
                    self.navigationController?.pushViewController(resetVC, animated: true)
                    
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Password reset code sent successfully", style: .success)
                    
                    
             }
                
                else {
                    
                    Indicator.shared.hideProgressView()
                    
                     _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Your Email / Phone Number doesn't exist with us.", style: .error)
                    
                }
                
            }, failure: { (error) in
                
            Indicator.shared.hideProgressView()
                
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
            
        }
        
        else {
            Indicator.shared.hideProgressView()
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            
            self.dismiss(animated: true, completion: nil)
            
        }
        
    }
    
}
