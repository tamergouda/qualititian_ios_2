//
//  LoginVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {


    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var lblSignup: UILabel!
    @IBOutlet weak var btnRemember: UIButton!
    
    //MARK:- Variables
    //MARK:-
    var validation: Validation = Validation.validationManager() as! Validation
    fileprivate var WebserviceConnection = AlmofireWrapper()

    
    //MARK:- View Life Cycle
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetupVC()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        if (UserDefaults.standard.value(forKey: "LOGIN_EMAIL") as? String ?? "" != "") {

            txtEmail.text = UserDefaults.standard.value(forKey: "LOGIN_EMAIL") as? String ?? ""
            txtPassword.text = UserDefaults.standard.value(forKey: "PASS_CODE") as? String ?? ""

            btnRemember.isSelected = true

        }

        else {

            txtEmail.text = ""
            txtPassword.text = ""

            btnRemember.isSelected = false

        }

    }
    
    //MARK:- Initial Set UP.
    //MARK:-
    func initialSetupVC() {
        
        let text = lblSignup.text
        let textRange = NSRange(location: 24, length: 6)
        let attributedText = NSMutableAttributedString(string: text!)
        
        attributedText.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 59.0/255, green: 208.0/255, blue: 214.0/255, alpha: 1.0), range: textRange)
        lblSignup.attributedText = attributedText
        
            //for Move To SignUp
        lblSignup.isUserInteractionEnabled = true // Remember to do this
        let tapLblSignUp: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(newToQaulitition))
        lblSignup.addGestureRecognizer(tapLblSignUp)
        
            //For Remember Me Button.
        btnRemember.setImage(UIImage(named : "add_member_un_selected"), for: UIControlState.normal)
        btnRemember.setImage(UIImage(named : "selected_tick"), for: UIControlState.selected)
        btnRemember.addTarget(self, action: #selector(myButtonTapped), for: UIControlEvents.touchUpInside)
        
    }
    
        //For Push To SignUp
    @objc func newToQaulitition(){
        
        let signUpType = self.storyboard?.instantiateViewController(withIdentifier: "SignUp_Type_VC") as! SignUp_Type_VC
        
        self.navigationController?.pushViewController(signUpType, animated: true)
        
    }
    
        //for Remember Me Button
    @objc func myButtonTapped(){
        
        if btnRemember.isSelected == true {
            btnRemember.isSelected = false
        }else {
            btnRemember.isSelected = true
            
        }
    }
    
    //MARK:- Actions Buttons
    //MARK:-
    @IBAction func btnForgotPass(_ sender: Any) {
        
        let forgorPassVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPassVC") as! ForgotPassVC
        
        self.navigationController?.pushViewController(forgorPassVC, animated: true)
        
    }
    
    @IBAction func btnLogin(_ sender: Any) {
     
        validationSetup()
        
        if btnRemember.isSelected == true {

            UserDefaults.standard.set(txtEmail.text, forKey: "LOGIN_EMAIL")
            UserDefaults.standard.set(txtPassword.text, forKey: "PASS_CODE")

        }else {

            UserDefaults.standard.removeObject(forKey: "LOGIN_EMAIL")
            UserDefaults.standard.removeObject(forKey: "PASS_CODE")

        }
        
    }
    
    
}

//MARK:- Custom Functions
//MARK:-
extension LoginVC{
    
    //MARK:- Validations
    //MARK:-
    func validationSetup()->Void{
        
        var message = ""
        
        if !validation.validateBlankField(txtEmail.text!){
            message = "Please enter your E-mail ID"
            
        }
        else if !validation.validateEmail(txtEmail.text!) && !validation.validatePhoneNumber(txtEmail.text!){
            
            message = "Please enter valid E-mail ID / Phone Number."
        }
            
        else if !validation.validateBlankField(txtPassword.text!){
            message = "Please enter password"
        }
        //        else if !validation.validatePassword(txtPassword.text!) {
        //
        //            message = "Invalid Password, Minimum 8 characters at least 1 Alphabet and 1 Number"
        //        }
        
        if message != "" {
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: AlertStyle.error)
            
        }else{
            loginAPICall()
        }
        
        
    }
    
}


//MARK:- Web Services API Call
//MARK:-
extension LoginVC {

    func loginAPICall() {
        
        let devicetoken = UserDefaults.standard.value(forKey: "devicetoken") as? String ?? "Simulaterwalatokenbb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326"
        
        let passDict = [
                            "email": txtEmail.text!,
                            "password": txtPassword.text!,
                            "devicetype":"iphone",
                            "devicetoken":devicetoken
            ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            
            WebserviceConnection.requestPOSTURL("login", params: passDict as [String : AnyObject], headers: nil, success: { (responseJson) in
                
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    
                    print("The Login Response is",responseJson)
                    
                    let dataDict = responseJson["login"].dictionaryValue
            
                    UserDefaults.standard.set(dataDict["token"]?.stringValue as? String ?? "", forKey: "responseToken")
                    
                    let userValue = dataDict["type_user"]?.stringValue as? String ?? "" //For Profile Restriction
                    let userTypeInInt = Int(userValue) as? Int ?? 0
                    
                    
                    let userName = dataDict["first_name"]?.stringValue as? String ?? "" // For Drawer Label
                    let userRole = dataDict["role"]?.stringValue as? String ?? "" //For BackUp Restriction
                    
                    let userID = dataDict["id"]?.stringValue as? String ?? "" //For webview
                    

                    UserDefaults.standard.set(userTypeInInt, forKey: "userType")
                    UserDefaults.standard.set(userName, forKey: "UserName")
                    UserDefaults.standard.set(userRole, forKey: "UserRole")
                    UserDefaults.standard.set(userID, forKey: "ResponseUserID")
                    
                     print(userTypeInInt)
                     UserDefaults.standard.synchronize()
                    
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initHome()
                    
                }
                
                else {
                    
                    let messgae = responseJson["message"].stringValue as? String ?? ""
                    print("Response Fail Message", messgae)
                    Indicator.shared.hideProgressView()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: messgae, style: .error)
                    
                }
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
            
        }
        else {
            
            Indicator.shared.hideProgressView()
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    
}
