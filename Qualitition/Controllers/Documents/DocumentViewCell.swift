//
//  DocumentViewCell.swift
//  Qualitition
//
//  Created by Callsoft on 13/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class DocumentViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgDocument: UIImageView!
    
    @IBOutlet weak var lblDocumentName: UILabel!
    
    
}
