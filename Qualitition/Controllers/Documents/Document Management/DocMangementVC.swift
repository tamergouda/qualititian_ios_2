//
//  DocMangementVC.swift
//  Qualitition
//
//  Created by Callsoft on 12/10/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class DocMangementVC: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func btnApproved(_ sender: Any) {
        
        let approvedVC = self.storyboard?.instantiateViewController(withIdentifier: "ApprovedDocVC") as! ApprovedDocVC
        
        self.navigationController?.pushViewController(approvedVC, animated: true)
    }
    
    @IBAction func btnPDF(_ sender: Any) {
        
        let pdfVC = self.storyboard?.instantiateViewController(withIdentifier: "PdfMangementVC") as! PdfMangementVC
        
        self.navigationController?.pushViewController(pdfVC, animated: true)
    }
    
    @IBAction func btnPending(_ sender: Any) {
        
        let pendingVC = self.storyboard?.instantiateViewController(withIdentifier: "PendingDocView") as! PendingDocView
        
        self.navigationController?.pushViewController(pendingVC, animated: true)
    }
    
    @IBAction func btnBackToDocument(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}
