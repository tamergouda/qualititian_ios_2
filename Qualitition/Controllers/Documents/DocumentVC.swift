//
//  DocumentVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class DocumentVC: UIViewController {
    
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var btnSort: UIButton!
    @IBOutlet weak var sortView: UIView!
    
    //******** Filter View Outlets
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var userFiltrTxt: UITextField!
    @IBOutlet weak var policyFiltrTxt: UITextField!
    @IBOutlet weak var manualFiltrTxt: UITextField!
    @IBOutlet weak var standardFiltrTxt: UITextField!
    @IBOutlet weak var departmentFiltrTxt: UITextField!
    @IBOutlet weak var publishFiltrTxt: UITextField!
    
    
    
    fileprivate var documentArray = NSArray()
    
    fileprivate var webServicesConnection = AlmofireWrapper()
    fileprivate let token = UserDefaults.standard.value(forKey: "responseToken")
    
    //************ DocPic
    let documentInteractionController = UIDocumentInteractionController()
    
    //******* SearchBar
    var searchActive = false
    var filtered : NSMutableArray = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        documentApiCall()
        documentInteractionController.delegate = self
        
        sortView.isHidden = true
        self.collectionView.addSubview(self.refreshControl)
        btnSort.addTarget(self, action: #selector(btnSortTabbed), for: .touchUpInside)
        
        filterView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        search.text = ""
        searchActive = false
        collectionView.reloadData()
        filterView.isHidden = true
    }
    
    
    @IBAction func btnBackToHome(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //This is header Button at Right Side.
    @IBAction func btnGoDocuModule(_ sender: Any) {
        //For WebView
//                let docManagment = self.storyboard?.instantiateViewController(withIdentifier: "DocMangementVC") as! DocMangementVC
//
//                self.navigationController?.pushViewController(docManagment, animated: true)
        
    }
    
    //Header Sort Button
    @objc func btnSortTabbed() {
        
//        if btnSort.isSelected == true {
//            sortView.isHidden = false
//            btnSort.isSelected = false
//        }else {
//            sortView.isHidden = true
//            btnSort.isSelected = true
//
//        }
        
        filterView.isHidden = false
        userFiltrTxt.isHidden = false
        policyFiltrTxt.isHidden = false
        manualFiltrTxt.isHidden = false
        standardFiltrTxt.isHidden = false
        departmentFiltrTxt.isHidden = false
        publishFiltrTxt.isHidden = false
    }
    
    
    
    @IBAction func btngoTODocModule(_ sender: Any) {
        //Go to Module Screen
        let docVC = self.storyboard?.instantiateViewController(withIdentifier: "DocModuleVC") as! DocModuleVC
        
        self.navigationController?.pushViewController(docVC, animated: false)
        
    }
    
    
    @IBAction func btnAlphabeticallyFilter(_ sender: Any) {
        
  //      print("Alphabetical Button OK")
        alert(message: "OK")
    }
    
    @IBAction func btnPublieshFilter(_ sender: Any) {
        
    //    print("Publish Button OK")
        
        alert(message: "OK")
    }
    
    
    @IBAction func btnFilterApply(_ sender: Any) {
        
        filterView.isHidden = true
        //Call the Filter API Here
    }
    
    @IBAction func btnCloseFilter(_ sender: Any) {
        filterView.isHidden = true
    }
    
    
    
    //INBUILD Refresh Control.
    var refreshControl: UIRefreshControl {
        
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action: #selector(DocumentVC.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor(red: 59.0/255, green: 208.0/255, blue: 214.0/255, alpha: 1.0)
        
        return refreshControl
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        //Call API Here
        documentApiCall()
        
        self.collectionView.reloadData()
        refreshControl.endRefreshing()
    }
    
}


//MARK:- Search Delegate Functions
//MARK:-
extension DocumentVC: UISearchBarDelegate{
    
    // When button "Search" pressed on search Bar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.search.endEditing(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        if searchBar.text == "" {
            searchActive = false
        }
            
        else {
            searchActive = true
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        print("THE ENTER TEXT", searchText)
        filtered.removeAllObjects()
        
        if searchBar.text!.isEmpty {
            searchActive = false
        }
            
        else {
            
            searchActive = true
            
            if documentArray.count >= 1 {
                
                for index in 0...documentArray.count-1 {
                    
                    let dicResponse = documentArray.object(at: index) as? NSDictionary ?? [:]
                    let documentName = dicResponse.object(forKey: "document_title") as? String ?? ""
                    
                    if (documentName.lowercased().range(of: searchText.lowercased()) != nil) {
                        
                        filtered.add(dicResponse)
                    }
                }
            }
        }
        
        collectionView.reloadData()
    }
    
}


//MARK:- CollectionView Functions
//MARK:-
extension DocumentVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if searchActive == true {
            
            return filtered.count
        }
            
        else {
            return self.documentArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: DocumentViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentViewCell", for: indexPath) as! DocumentViewCell


        if searchActive == true {
            
            let dataDict = filtered.object(at: indexPath.row) as? NSDictionary ?? [:]
            
            cell.lblDocumentName.text = dataDict.value(forKey: "document_title") as? String ?? ""
            
            let responseImg = dataDict.value(forKey: "hospital_logo") as? String ?? ""
            cell.imgDocument.sd_setImage(with: URL(string: responseImg), placeholderImage: #imageLiteral(resourceName: "Default_logo"))
        }
            
        else {
            
            let dataDict = documentArray.object(at: indexPath.row) as? NSDictionary ?? [:]
            cell.lblDocumentName.text = dataDict.value(forKey: "document_title") as? String ?? ""
            let responseImg = dataDict.value(forKey: "hospital_logo") as? String ?? ""
            cell.imgDocument.sd_setImage(with: URL(string: responseImg), placeholderImage: #imageLiteral(resourceName: "Default_logo"))
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if searchActive == true {
            
            let dataDict = filtered.object(at: indexPath.row) as? NSDictionary ?? [:]
            let documenturl = dataDict.value(forKey: "file") as? String ?? ""
            
            if documenturl == "" {
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "No Document Found", style: .error)
            }
            else if documenturl != "" {
                
                storeAndShare(withURLString: documenturl)
            }
            else {
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Something went wrong", style: .error)
            }
            
            
        }
            
        else {
            
            let dataDict = documentArray.object(at: indexPath.row) as? NSDictionary ?? [:]
            let documentURL = dataDict.value(forKey: "file") as? String ?? ""
            
            if documentURL == "" {
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "No Document Found", style: .error)
            }
            else if documentURL != "" {
                storeAndShare(withURLString: documentURL)
            }
            else {
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Something went wrong", style: .error)
            }
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellsAcross: CGFloat = 2
        let spaceBetweenCells: CGFloat = 20
        let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
        
        return CGSize(width: dim, height: dim)
        
    }
    //Animate the Collection View cell
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.alpha = 0
        UIView.animate(withDuration: 0.8) {
            cell.alpha = 1
        }
    }
    //Animate the item inside the cell when you select the cell.
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = collectionView.cellForItem(at: indexPath) as? DocumentViewCell {
                cell.imgDocument.transform = .init(scaleX: 0.85, y: 0.85)
                cell.lblDocumentName.transform = .init(scaleX: 0.85, y: 0.85)
                cell.contentView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
            }
        }
    }
    //Remove the aniomation when you deselectg the cell, release the touch on cell.
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            UIView.animate(withDuration: 0.5) {
                if let cell = collectionView.cellForItem(at: indexPath) as? DocumentViewCell {
                    cell.imgDocument.transform = .identity
                    cell.lblDocumentName.transform = .identity
                    cell.contentView.backgroundColor = .clear
                }
            }
            
        }
        
    }
    
    
}

//MARK:- Web Services API Call
//MARK:-
extension DocumentVC{
    
    func documentApiCall() {
        
        let passDict = [
            "token": token,
            ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            webServicesConnection.requestPOSTURL("documentListing", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    Indicator.shared.hideProgressView()
                    
                    let documentDummyArray = self.documentArray.mutableCopy() as? NSMutableArray ?? []
                    documentDummyArray.removeAllObjects()
                    self.documentArray = documentDummyArray.mutableCopy() as? NSArray ?? []
                    
                    print("THE Document VC RESPONSE IS", responseJson)
                    
                    self.documentArray = responseJson["documentListing"].arrayObject as? NSArray ?? []
                    self.collectionView.reloadData()
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                    Indicator.shared.hideProgressView()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                    let message = responseJson["message"].stringValue
                    Indicator.shared.hideProgressView()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                }
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
            
        else {
            
            Indicator.shared.hideProgressView()
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func documentfilterApi() {
        
        let passDict = [
            "token": token,
            ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            webServicesConnection.requestPOSTURL("DocumentFilter", params: passDict as [String: AnyObject], headers: nil, success: { (responseJSON) in
                
                if responseJSON["status"].stringValue == "Success" {
                    Indicator.shared.hideProgressView()
                    print("THE FILTER RESPONSE IS", responseJSON)
                }
                else if responseJSON["message"].stringValue == "Invalid User." {
                    Indicator.shared.hideProgressView()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                
                else {
                    let message = responseJSON["message"].stringValue
                    Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                    
                }
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
        else {
            
            Indicator.shared.hideProgressView()
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}

//MARK:- Document Interaction Controller
//MARK:-
extension DocumentVC {
    /// This function will set all the required properties, and then provide a preview for the document
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.presentPreview(animated: true)
    }
    
    /// This function will store your document to some temporary URL and then provide sharing, copying, printing, saving options to the user
    func storeAndShare(withURLString: String) {
        guard let url = URL(string: withURLString) else { return }
        /// START YOUR ACTIVITY INDICATOR HERE
        Indicator.shared.showProgressView(self.view)
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else { return }
            let tmpURL = FileManager.default.temporaryDirectory
                .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
            do {
                try data.write(to: tmpURL)
            } catch {
                print(error)
            }
            DispatchQueue.main.async {
                /// STOP YOUR ACTIVITY INDICATOR HERE
                Indicator.shared.hideProgressView()
                self.share(url: tmpURL)
            }
            }.resume()
    }
}

//MARK:- Document Interaction Delegate
//MARK:-
extension DocumentVC: UIDocumentInteractionControllerDelegate {
    /// If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}

