//
//  TestAppType.swift
//  dropBoxLearning
//
//  Created by Callsoft on 17/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import Foundation

public enum AppPermission {
    case fullDropbox
    case teamMemberFileAccess
    case teamMemberManagement
}

let appPermission = AppPermission.fullDropbox
