//
//  BackupVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleAPIClientForREST
import SwiftyDropbox
import MobileCoreServices

class BackupVC: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate, UIDocumentPickerDelegate,UINavigationControllerDelegate, UIDocumentMenuDelegate {

    
    @IBOutlet weak var btnDriveFileBrowser: UIButton!
    @IBOutlet weak var btnGoogleFileBrowser: UIButton!
    
    @IBOutlet weak var btnSignOutDropBox: UIButton!
    @IBOutlet weak var btnSignOutGoogle: UIButton!
    
    
    //****** For GoogleDrive ******
    fileprivate let service = GTLRDriveService()
    private var drive: ATGoogleDrive?
    
    var backUpTo = ""  // for check inside the didPick func
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getTheCurrentUserInfoDropBox()  // Only for DropBox
        
        self.btnSignOutGoogle.isHidden = true
        self.btnSignOutDropBox.isHidden = true
        self.btnGoogleFileBrowser.isHidden = true
        self.btnDriveFileBrowser.isHidden = true
        drive = ATGoogleDrive(service)
    }


    @IBAction func btnBackToHome(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnDriveLink(_ sender: Any) {
        self.backUpTo = "GoogleDrive"
        setupGoogleSignIn()
    }
    
    @IBAction func btnDropBoxLink(_ sender: Any) {

        self.backUpTo = "DropBox"
            //Authorizing the User to the DropBox
        if (DropboxClientsManager.authorizedClient == nil) {
            
            DropboxClientsManager.authorizeFromController(UIApplication.shared, controller: self, openURL: {(url: URL) -> Void in UIApplication.shared.openURL(url)})
        }
            
        else {
            print("User already authorized")
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Succesfully Signed In", style: .success)
            btnSignOutDropBox.isHidden = false
            btnDriveFileBrowser.isHidden = false
        }
    }
    
        //SignOut Button of DropBox
    @IBAction func btnDropBoxSignOut(_ sender: Any) {
        DropboxClientsManager.unlinkClients()  //LogOut From DropBox
        self.backUpTo = ""
        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Sign Out Successfully", style: .success)
        
        btnDriveFileBrowser.isHidden = true
        btnSignOutDropBox.isHidden = true
    }

    
        //Browese Button
    @IBAction func btnUploadToDropBox(_ sender: Any) {
            //Open the Doc Picker
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeRTF), String(kUTTypePNG), String(kUTTypeJPEG), String(kUTTypeURL), String(kUTTypeText),String(kUTTypeSpreadsheet),String(kUTTypePresentation), "com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document"], in: .import)

        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    @IBAction func btnUploadToDrive(_ sender: Any) {
            //Open the Doc Picker
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeRTF), String(kUTTypePNG), String(kUTTypeJPEG), String(kUTTypeURL), String(kUTTypeText),String(kUTTypeSpreadsheet),String(kUTTypePresentation), "com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document"], in: .import)
        
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    @IBAction func btnSignOutGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance().signOut() //SignOut from Google
        self.backUpTo = ""
        _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Sign Out Successfully", style: .success)
       
        btnGoogleFileBrowser.isHidden = true
        btnSignOutGoogle.isHidden = true
    }
    
        //Permission Authorizer For Google Drive
    private func setupGoogleSignIn() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = [kGTLRAuthScopeDriveFile]
      //  GIDSignIn.sharedInstance().signInSilently()
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        if let documentsDir = url as URL? {
            
            print("import result : \(documentsDir)")
            
            let indexFileName = (documentsDir.pathComponents.last)!
            print("THE FILE NAME", indexFileName)
            
            let testFilePath = documentsDir.appendingPathComponent("").path

            if (self.backUpTo == "GoogleDrive") {
                if InternetConnection.internetshared.isConnectedToNetwork() {
                    Indicator.shared.showProgressView(self.view)
                    
                        //For Drive BackUp
                    drive?.uploadFile("Qualititions", filePath: testFilePath, MIMEType: "image/pdf") { (fileID, error) in
                        print("Upload file ID: \(String(describing: fileID)); Error: \(String(describing: error?.localizedDescription))")
                        
                        print("THE FILE ID IS",fileID ?? "")
                        print("The test File Path", testFilePath)
                        
                        if fileID != "" {
                                Indicator.shared.hideProgressView()
                            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "File Successfully Uploaded", style: .success)
                        }
                        else {
                                Indicator.shared.hideProgressView()
                            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                        }
                        
                    }
                    
                }
                else {
                    
                        Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
                        self.dismiss(animated: true, completion: nil)
                }
                
                
            }
            else if (self.backUpTo == "DropBox") {
                    //DropBox BackUp
                let client = DropboxClientsManager.authorizedClient
                let data = try! Data(contentsOf: documentsDir)
                
                if InternetConnection.internetshared.isConnectedToNetwork() {
                    Indicator.shared.showProgressView(self.view)
                    
                    let request = client?.files.upload(path: "/Qualitition/\(indexFileName)", input: data)
                        .response { response, error in
                            if let response = response {
                                
                                Indicator.shared.hideProgressView()
                                
                                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "File Successfully Uploaded", style: .success)
                                
                                print("The response is Following",response)
                                
                            } else if let error = error {
                                Indicator.shared.hideProgressView()
                                print(error)
                            }
                        }
                        .progress { progressData in
                            print("The progress Data is",progressData)
                    }
                    
                    
                }
                    
                else {
                    Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
        }
        
    }
    
    func getTheCurrentUserInfoDropBox() {
        
        if let client = DropboxClientsManager.authorizedClient {
            
            client.users.getCurrentAccount().response { response, error in
                
                if let account = response {
                    
                    print("Hello \(account.name.givenName)")
                    
                }
                else {
                    print("THERE HAVE BEEN ERROR OCCURE")
                }
                
            }
            
        }
        
    }
    
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- GIDSignInDelegate
//MARK:-
extension BackupVC {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let _ = error {
            service.authorizer = nil
            print("THIS SHOULD BE FAIL IF USER NOT SIGN IN")
        } else {
            service.authorizer = user.authentication.fetcherAuthorizer()
            print("USER SIGNED IN")
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Succesfully Signed In", style: .success)
            self.btnGoogleFileBrowser.isHidden = false
            self.btnSignOutGoogle.isHidden = false
        }
    }
    
}
