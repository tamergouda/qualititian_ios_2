//
//  ResetOTPVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class ResetOTPVC: UIViewController {

    @IBOutlet weak var otpView: VPMOTPView!
    @IBOutlet weak var changePassView: UIView!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    var enteredOtp: String = ""
    var OTPVALUE: String = ""
    fileprivate var webServicesConnection = AlmofireWrapper()
    fileprivate var validation:Validation = Validation.validationManager() as! Validation
    
    //MARK:- View Life Cycle
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()

        print("THE OTP VALUE IN RESET VC",OTPVALUE)
        
        changePassView.isHidden = true
        txtNewPassword.isHidden = true
        txtConfirmPassword.isHidden = true
        
        otpView.delegate = self
        settingOtpView()
        
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
        print(enteredOtp)
        print(OTPVALUE)
        
        if (OTPVALUE == enteredOtp) {

            print("Call the Change pass API")
            //Called that api at validation of submit button.
            animateChangePassView()
        }
        else {

            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Please Enter Valid OTP", style: AlertStyle.error)
        }
        
    }
    
    @IBAction func btnResend(_ sender: Any) {
        resendOtpApiCall()
    }

    @IBAction func btnBackToForgetPassOrSignUp(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnClosePassView(_ sender: Any) {
            self.changePassView.isHidden = true
    }
    
    @IBAction func btnSubmitPassword(_ sender: Any) {
            validationSetup()
    }
    
    
}


//MARK:-Custom Methods
//MARK:-
extension ResetOTPVC{
    
    func settingOtpView() {
        
        //        otpView.shouldAllowIntermediateEditing = false
        
        otpView.otpFieldsCount = 4
        otpView.otpFieldDisplayType = .underlinedBottom
        otpView.otpFieldBorderWidth = 0
        otpView.otpFieldInputType = .numeric
        otpView.otpFieldDefaultBorderColor = UIColor.lightGray
        otpView.otpFieldEnteredBorderColor = UIColor(red: 0/255, green: 166/255, blue: 177/255, alpha: 1.0)
        otpView.cursorColor = UIColor(red: 0/255, green: 166/255, blue: 177/255, alpha: 1.0)
        otpView.otpFieldSeparatorSpace = 20
        otpView.otpFieldSize = 50
        otpView.delegate = self
        
        otpView.initalizeUI()
        
    }
    
    func animateChangePassView() {
        changePassView.isHidden = false
        txtNewPassword.isHidden = false
        txtConfirmPassword.isHidden = false
        
        self.changePassView.transform = CGAffineTransform(translationX: -400, y: 0)
        self.txtNewPassword.transform = CGAffineTransform(translationX: -400, y: 0)
        self.txtConfirmPassword.transform = CGAffineTransform(translationX: -400, y: 0)
        
        UIView.animate(withDuration: 0.5) {
            self.changePassView.transform = CGAffineTransform.identity
            self.txtNewPassword.transform = CGAffineTransform.identity
            self.txtConfirmPassword.transform = CGAffineTransform.identity
        }
    }
    
    //TODO:- Validations
    func validationSetup()->Void{
        
        var message = ""

        
        if !validation.validateBlankField(txtNewPassword.text!){
            message = "Please enter New Password"
        }
            
        else if !validation.validateBlankField(txtConfirmPassword.text!) {
            message = "Please enter Confirm Password"
        }
            
        else if txtNewPassword.text! != txtConfirmPassword.text!{
            message = "Password and Confirm Password must be same"
        }
        
        if message != "" {
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: AlertStyle.error)
            
        }else{
            
            updatePasswordApiCall()
            
        }
        
        
    }

    
}

//MARK:- VPMOTP Delegates
//MARK:-
extension ResetOTPVC: VPMOTPViewDelegate {
    
    func hasEnteredAllOTP(hasEntered: Bool) {
        
        print("")
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
       
        print("Has entered all OTP? \(hasEntered)")
        
        //return enteredOtp == "12345"
        return true
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        //enteredOtp = otpString
        //   signUserAPI()
        
        print("OTPString: \(otpString)")
        
        enteredOtp = otpString
        
    }
}

//MARK:- Web Services API Call
//MARK:-
extension ResetOTPVC {
    
    func updatePasswordApiCall() {
        
        let email = UserDefaults.standard.value(forKey: "otpRecoverEmail") as? String ?? ""
        
        let passDict = [
            "email": email,
            "new_password": txtNewPassword.text!,
            "confirm_password": txtConfirmPassword.text!,

            ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            
            webServicesConnection.requestPOSTURL("updatePassword", params: passDict as [String : AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    
                    print("password Change Successfully, Now please login") //Redirect it to loginScreen
                    
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    
                    print("AT RESET", responseJson)
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Password changed successfully", style: .success)
                }
                
                else {
                    
                    Indicator.shared.hideProgressView()
                    
                    let responseMessgae = responseJson["message"].stringValue
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: responseMessgae, style: .error)
                    
                    
                }
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
        
        else {
            
            Indicator.shared.hideProgressView()
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func resendOtpApiCall() {
        
        let email = UserDefaults.standard.value(forKey: "otpRecoverEmail") as? String ?? ""
        
        let passDict = [
            "email": email //Need to have the phone number also
            ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            
            webServicesConnection.requestPOSTURL("forgotPassword", params: passDict as [String : AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    
                    print("THE RESPONSE",responseJson)
                    
                    let dataDict = responseJson["forgotPassword"].dictionaryObject as? NSDictionary ?? [:]
                    
                    print("THE DICTIONART", dataDict)
                    

                    self.OTPVALUE = dataDict.value(forKey: "code") as? String ?? ""
                    
                    print("THE RESEND OTP", self.OTPVALUE)
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Your password has been sent on your email Id.", style: .success)
                    
                    
                }
                    
                else {
                    
                    Indicator.shared.hideProgressView()
                    
                    let responseMessage = responseJson["message"].stringValue
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: responseMessage, style: .error)
                    
                }
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
            
        }
            
        else {
            Indicator.shared.hideProgressView()
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            
            self.dismiss(animated: true, completion: nil)
            
        }
        
    }
        
    
}
