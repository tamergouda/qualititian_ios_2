//
//  DrawerController.swift
//  Qualitition
//
//  Created by Callsoft on 11/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class DrawerController: UIViewController, UITableViewDataSource, UITableViewDelegate {


    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Variables
    //MARK:-
    let arrayTitles = ["  Notification","About Us","  Contact Us","   Logout"]
    let arrayImage = [#imageLiteral(resourceName: "side_notification"),#imageLiteral(resourceName: "side_about_us"),#imageLiteral(resourceName: "side_contactus"),#imageLiteral(resourceName: "side_logout")]
    
    var webServiceConnection = AlmofireWrapper()
    
    
    //MARK:- View Life Cycle
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawerNameSetUp()
        tableView.tableFooterView = UIView() //remove the line seperator below the field
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("View will appear of Drawer")
        drawerNameSetUp()
        
    }
    
    //MARK:- Table View Functions
    //MARK:-
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.selectionStyle = .none
        cell?.textLabel?.text = arrayTitles[indexPath.row]
        cell?.textLabel?.textColor = UIColor.darkGray
        cell?.imageView?.image = arrayImage[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let appDel = UIApplication.shared.delegate as! AppDelegate

        switch indexPath.row {
        case 0:
            let notification = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
            appDel.drawerController.mainViewController = notification
            break
        case 1:
            let aboutUs = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
            appDel.drawerController.mainViewController = aboutUs
            break
        case 2:
            let contactUs = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            appDel.drawerController.mainViewController = contactUs
            break
        default:
            
            LOGOUT()
            print("LOGOUT")
        }

        appDel.drawerController.setDrawerState(.closed, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 55
    }
    
    //MARK:- Custom Functions
    //MARK:-
    func LOGOUT() {
        
        SweetAlert().showAlert("Qualitions", subTitle: "Are you sure?\nYou want to logout!", style: AlertStyle.warning, buttonTitle:"Cancel", buttonColor:UIColor.colorFromRGB(0xD0D0D0) , otherButtonTitle:  "OK", otherButtonColor: UIColor.colorFromRGB(0xDD6B55)) { (isOtherButton) -> Void in
            if isOtherButton == true {
                
                print("Cancel Button  Pressed", terminator: "")
            }
            else
            {
                self.logoutAPICall()
            }
            
        }
        
    }
    
    func logoutAPICall() {
        
        let token = UserDefaults.standard.value(forKey: "responseToken")
        
        let passDict = ["token": token] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            
            webServiceConnection.requestPOSTURL("logout", params: passDict as [String : AnyObject], headers: nil, success: { (reponseJson) in
                
                Indicator.shared.hideProgressView()
                
                print("THE USER IS LOGOUT", reponseJson)
                
                UserDefaults.standard.removeObject(forKey: "responseToken")
               
                let appDel = UIApplication.shared.delegate as! AppDelegate
                appDel.initLoginAtLogOut()
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
        
        else {
            
            Indicator.shared.hideProgressView()
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func drawerNameSetUp(){
        
        let first_name = UserDefaults.standard.value(forKey: "UserName") as? String ?? ""
      //  print("Set value in Drawer Controller, from Profile and Login", first_name)
        self.lblName.text = first_name
        
    }
    
    
}
