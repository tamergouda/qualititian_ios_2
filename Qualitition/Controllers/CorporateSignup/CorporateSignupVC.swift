//
//  CorporateSignupVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import DropDown

class CorporateSignupVC: UIViewController, UITextFieldDelegate {


    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var txtCorporateName: UITextField!
    @IBOutlet weak var txtCorporateEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtTypeofCorporate: UITextField!
    @IBOutlet weak var txtRole: UITextField!
    @IBOutlet weak var txtDesignation: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    

    var countryCodeArray:NSMutableArray = NSMutableArray()
    var arrayFromPlist:NSMutableArray = NSMutableArray()
    var validation: Validation = Validation.validationManager() as! Validation
    let dropdown = DropDown()
    var WebserviceConnection = AlmofireWrapper()
    
    var roleIndex = ""  // For Role Index
    
    
    //MARK:- View Life Cycle
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetupVC()
    }


        //For Country Picker
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        loadPlistDataatLoadTime()
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String
        {
            for i in 0 ..< self.arrayFromPlist.count
            {
                if let dic = self.arrayFromPlist.object(at: i) as? NSDictionary
                {
                    if countryCode == dic.object(forKey: "country_code") as! String {
                        
                        lblCountryCode.text = dic.object(forKey: "country_dialing_code") as? String ?? ""
                        
                        txtCountry.text = dic.object(forKey: "country_name") as? String ?? ""
                        
                    }
                }
            }
        }
    }
    

    @IBAction func btnCountryCode(_ sender: Any) {
        
     //   showCountryPicker()
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        
        validationSetup()
    }
    
    @IBAction func btnCountrySelect(_ sender: Any) {
        
        showCountryPicker()
    }
    
    @IBAction func btnRoleDropDown(_ sender: Any) {
        
        roleDropDown()
    }
    
    @IBAction func btnDesignationDropDown(_ sender: Any) {
    }
    
    @IBAction func btnBackToSignUpType(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

        //Initial Set UP.
    func initialSetupVC() {
        
        let text = lblLogin.text
        let textRange = NSRange(location: 24, length: 6)
        let attributedText = NSMutableAttributedString(string: text!)
        
        attributedText.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 59.0/255, green: 208.0/255, blue: 214.0/255, alpha: 1.0), range: textRange)
        lblLogin.attributedText = attributedText
        
        lblLogin.isUserInteractionEnabled = true // Remember to do this
        let tapLblLogin: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(newToQaulitition))
        lblLogin.addGestureRecognizer(tapLblLogin)
        
        
        txtCorporateName.delegate = self
        
    }
    
    @objc func newToQaulitition(){
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.initLoginAtLogOut() 
    }
    
    
}

//MARK:- Custom Functions
//MARK:-
extension CorporateSignupVC {
    
        //To load the data from infoPlist of Country
    func loadPlistDataatLoadTime() {
        
        //getting path to GameData.plist
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentsDirectory = paths[0] as! NSString
        let path = documentsDirectory.appendingPathComponent("countryList.plist")
        let fileManager = FileManager.default
        
        //check if file exists
        if (!fileManager.fileExists(atPath: path)) {
            
            //if it doesn't, copy it from the default file in the Bundle
            if let bundlePath = Bundle.main.path(forResource: "countryList", ofType: "plist") {
                let rootArray = NSMutableArray(contentsOfFile: bundlePath)
                //    print("Bundle RecentSearch.plist file is --> \(rootArray?.description)")
                
                do {
                    try fileManager.copyItem(atPath: bundlePath, toPath: path)
                }
                catch _ {
                    print("Fail to copy")
                }
                print("copy")
            } else {
                print("RecentSearch.plist not found. Please, make sure it is the part of the Bundle.")
            }
        } else {
            print("RecentSearch.plist already exits at path.")
        }
        
        
        let rootarray = NSMutableArray(contentsOfFile: path)
        //    print("Loaded RecentSearch.plist file is --> \(rootarray?.description)")
        let array = NSMutableArray(contentsOfFile: path)
        
        if let dict = array {
            
            let tempArray = array!
            self.arrayFromPlist = tempArray
            var i = 0
            for index in tempArray {
                
                let dic = tempArray.object(at: i) as? NSDictionary
                i = i+1
                let code = dic?.object(forKey: "country_dialing_code") as? String
                let trimString:String = code! .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                let countryName = dic?.object(forKey: "country_name") as? String
                let codeString = trimString+" "+countryName!
                
                self.countryCodeArray.add(codeString)
            }
            //print(self.countryCodeArray)
            
        } else {
            print("WARNING: Couldn't create dictionary from RecentSearch.plist! Default values will be used!")
        }
        
    }
    
        //showing the Country Picker
    func showCountryPicker()
    {
        
        self.view.endEditing(true)
        //print(self.countryCodeArray)
        
        ActionSheetStringPicker.show(withTitle: "", rows: self.countryCodeArray as [AnyObject], initialSelection: 76, doneBlock: { (picker,selectedIndex, origin) -> Void in
            
            let select = selectedIndex
            print(select)
            let dic = self.arrayFromPlist.object(at: select) as? NSDictionary
            
            self.lblCountryCode.text = dic?.object(forKey: "country_dialing_code") as? String
            
            self.txtCountry.text = dic?.object(forKey: "country_name") as? String
            
        }, cancel: { (picker) -> Void in
            
        }, origin: self.view)
        
    }
    
    //for Role DropDown
    func roleDropDown() {
        
        dropdown.show()
        
        dropdown.dataSource = ["Owner", "Director", "Manager", "Specialist", "Champion", "Achiever", "Team"]
        dropdown.direction = .bottom
        dropdown.anchorView = self.txtRole as! AnchorView
        dropdown.bottomOffset = CGPoint(x: 0, y:txtRole.bounds.height)
        
        // Action triggered on selection of dropdown
        dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            print("THE INDEX", index + 1 )
            self.roleIndex = "\(index + 1)"
            
            self.txtRole.text = "\(item)"
        }
        
    }
    
    //For alphabets Only
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let texts = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ. ")
        
        if textField == txtCorporateName
        {
            guard let text = txtCorporateName.text else { return true }
            
            let newLength = text.characters.count + string.characters.count
                - range.length
            return newLength <= 50 && (txtCorporateName.text! + string).rangeOfCharacter(from: texts.inverted) == nil
        }
        
        return true
    }
    
    //MARK:- Validations
    //MARK:-
    func validationSetup()->Void{
        
        var message = ""
        
        if !validation.validateBlankField(txtCorporateName.text!){
            
            message = "Please enter your Corporate Name"
        }
            
        else if !validation.validateBlankField(txtCorporateEmail.text!){
            message = "Please enter Corporate E-mail ID"
            
        }else if !validation.validateEmail(txtCorporateEmail.text!){
            
            message = "Please enter valid Corporate E-mail ID"
        }
        else if !validation.validateBlankField(txtPassword.text!){
            message = "Please enter Password"
        }
            
        else if !validation.validateBlankField(txtConfirmPassword.text!) {
            
            message = "Please enter Confirm Password"
        }
            
        else if txtPassword.text! != txtConfirmPassword.text!{
            
            message = "Password and Confirm Password must be same"
            
        }
            
        else if !validation.validateBlankField(txtTypeofCorporate.text!){
            message = "Please Select the type of Corporate"
        }
            
        else if !validation.validateBlankField(txtRole.text!){
            message = "Please select Role"
        }
            
        else if !validation.validateBlankField(txtDesignation.text!){
            message = "Please Select the Designation"
        }
            
        else if !validation.validateBlankField(txtCountry.text!){
            message = "Please Select the Country"
        }
            
        else if !validation.validateBlankField(txtPhoneNumber.text!) {
            
            message = "Please enter Phone Number"
        }
        
        
        if message != "" {
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: AlertStyle.error)
            
        }else{
            clinetSignUpApiCall()
        }
        
        
    }
    
}

//MARK:- CLIENT SIGNUP API CALL
//MARK:-
extension CorporateSignupVC {
    
    func clinetSignUpApiCall() {
        
        let devicetoken = UserDefaults.standard.value(forKey: "devicetoken") as? String ?? "Simulaterwalatokenbb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326"
        
        let userType = UserDefaults.standard.value(forKey: "clientUser")
        
        let passDict = [
                            "first_name":txtCorporateName.text!,
                            "email":txtCorporateEmail.text!,
                            "type_corprate": txtTypeofCorporate.text!,
                            "role": self.roleIndex,  // Sending Index Value of dropdown
                            "designation": txtDesignation.text!,
                            "country": txtCountry.text!,
                            "country_code": lblCountryCode.text!,
                            "password": txtPassword.text!,
                            "confirm_password": txtConfirmPassword.text!,
                            "mobile": txtPhoneNumber.text!,
                            "type_user": userType ?? 52,
                            "devicetype":"iphone",
                            "devicetoken":devicetoken
            
            ] as [String : Any]
        
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            
            WebserviceConnection.requestPOSTURL("clientSignup", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    
                    print("_________________")
                    print("Client SignUp Response",responseJson)
                    print("_________________")
                    
                    let dataDict = responseJson["clientSignup"].dictionaryValue
                    
                    let userName = dataDict["first_name"]?.stringValue as? String ?? ""
                    let userToken = dataDict["token"]?.stringValue as? String ?? ""
                    let userRole = dataDict["role"]?.stringValue as? String ?? ""
                    let userID = dataDict["id"]?.stringValue as? String ?? ""
                    
                    
                    UserDefaults.standard.set(userName, forKey: "UserName")
                   
                    let userValue = 52
                    UserDefaults.standard.set(userValue, forKey: "userType") //overrite UserValue coming from Login Response
                    UserDefaults.standard.set(userToken, forKey: "responseToken") //overide the token from login to show at profile
                    UserDefaults.standard.set(userRole, forKey: "UserRole") //overrite UserRole coming from Login Response
                    UserDefaults.standard.set(userID, forKey: "ResponseUserID") //overrite UserId coming from Login Response
                    
                    UserDefaults.standard.synchronize()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initHome()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "You have registered successfully.", style: .success)
                }
                    
                else {
                    
                    let message = responseJson["message"].stringValue
                    Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                    
                }
                
            }, failure: { (error) in
                
                    Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
            
        }
            
        else {
            
            Indicator.shared.hideProgressView()
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
}
