//
//  HomeViewController.swift
//  Qualitition
//
//  Created by Callsoft on 13/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController{
    
    @IBOutlet weak var collectionView: UICollectionView!

    
    let homeName = ["USER", "DOCUMENT", "PRINT", "BACKUP", "ANALYTICS", "PROFILE"]
    let homeImage = [#imageLiteral(resourceName: "home_user"), #imageLiteral(resourceName: "home_document"), #imageLiteral(resourceName: "home_print"), #imageLiteral(resourceName: "home_backup"), #imageLiteral(resourceName: "home_analytics"), #imageLiteral(resourceName: "home_profile")]
    fileprivate var webServicesConnection = AlmofireWrapper()
    
    //MARK:- View Life Cycle
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //    print(UserDefaults.standard.value(forKey: "responseToken"))
        
    }
    
    @IBAction func menuBtn(_ sender: Any) {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.drawerController.setDrawerState(.opened, animated: true)
    }
    
}


//MARK:- Collection View Functions
//MARK:-
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return homeName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:HomeViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeViewCell", for: indexPath) as! HomeViewCell
        
        
        cell.imgHome.image = homeImage[indexPath.row]
        cell.lblHome.text = homeName[indexPath.row]
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            restrictUserManagement()
            
            //            let userVC = self.storyboard?.instantiateViewController(withIdentifier: "UserMangementVC") as! UserMangementVC
            //            self.navigationController?.pushViewController(userVC, animated: true)
            
        }
        else if indexPath.row == 1 {
            
            let docVC = self.storyboard?.instantiateViewController(withIdentifier: "DocumentVC") as! DocumentVC
            
            self.navigationController?.pushViewController(docVC, animated: true)
            
        }
        else if indexPath.row == 2 {
            
            let prntVC = self.storyboard?.instantiateViewController(withIdentifier: "PrintVC") as! PrintVC
            
            self.navigationController?.pushViewController(prntVC, animated: true)
            
        }
            
        else if indexPath.row == 3 {
            restrictOnBackUp()
        }
            
        else if indexPath.row == 4 {
            
            let analyticVC = self.storyboard?.instantiateViewController(withIdentifier: "AnalyticsVC") as! AnalyticsVC
            
            self.navigationController?.pushViewController(analyticVC, animated: true)
            
        }
            
        else if indexPath.row == 5 {
            restrictUser()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellsAcross: CGFloat = 2
        let spaceBetweenCells: CGFloat = 20
        let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
        
        return CGSize(width: dim, height: dim)
        
    }
    
}

//MARK:- Custom Functions
//MARK:-
extension HomeViewController{
    
    func restrictUser() {
        
        if (UserDefaults.standard.value(forKey: "userType")! as? Int) == 51 {
            
            print("This is Guest")
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "You are not authorized", style: .error)
        }
            
        else if (UserDefaults.standard.value(forKey: "userType")! as? Int) == 52 {
            
            print("This is Client")
            let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        
    }
    
    func restrictUserManagement() {
        
        if (UserDefaults.standard.value(forKey: "userType")! as? Int) == 51 {
            
            print("This is Guest")
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "You are not authorized", style: .error)
        }
            
        else if (UserDefaults.standard.value(forKey: "userType")! as? Int) == 52 {
            
            print("This is Client")
            let userVC = self.storyboard?.instantiateViewController(withIdentifier: "UserMangementVC") as! UserMangementVC
            
            self.navigationController?.pushViewController(userVC, animated: true)
        }
        
    }
    
    func restrictOnBackUp() {
        
        if (UserDefaults.standard.value(forKey: "UserRole")! as? String) == "Owner" {
            
            let backVC = self.storyboard?.instantiateViewController(withIdentifier: "BackupVC") as! BackupVC
            
            self.navigationController?.pushViewController(backVC, animated: true)
            
        }
            
        else if (UserDefaults.standard.value(forKey: "UserRole")! as? String) != "Owner" {
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "You are not authorized", style: .error)
        }
        
    }
    
}

//MARK:- Web Services API Call
//MARK:-
extension HomeViewController{
    
    func homeServiceApiCall() {
        
        let devicetoken = UserDefaults.standard.value(forKey: "devicetoken") as? String ?? "Simulaterwalatokenbb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326"
        
        let passDict = [
            "token":"okokokokok",
            "devicetoken": devicetoken
            ] as [String: Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            
            webServicesConnection.requestPOSTURL("home", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    
                    print(responseJson)
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                    Indicator.shared.hideProgressView()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                    let message = responseJson["message"].stringValue
                    
                    Indicator.shared.hideProgressView()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                }
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
        else {
            
            Indicator.shared.hideProgressView()
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            
            self.dismiss(animated: true, completion: nil)
            
        }
        
    }
    
    
}
