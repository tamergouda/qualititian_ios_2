//
//  HomeViewCell.swift
//  Qualitition
//
//  Created by Callsoft on 13/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class HomeViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var lblHome: UILabel!
    
    
}
