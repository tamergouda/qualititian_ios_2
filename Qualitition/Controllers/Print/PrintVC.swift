//
//  PrintVC.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit
import SDWebImage

class PrintVC: UIViewController {
    
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var btnSort: UIButton!
    
    //******* Filter View Outlets
    @IBOutlet weak var filterView: UIView!
    
    @IBOutlet weak var docNameFiltr: UITextField!
    @IBOutlet weak var manualFitrTxt: UITextField!
    @IBOutlet weak var standardFiltrTxt: UITextField!
    @IBOutlet weak var departmentFiltrTxt: UITextField!
    
    //*********** Variables
    fileprivate let webServicesConnection = AlmofireWrapper()
    fileprivate let token = UserDefaults.standard.value(forKey: "responseToken")
    fileprivate var printDataArray = NSArray()  //For the Response
    fileprivate var DocumentUrlArray = NSMutableArray()  //For response Doc Url
    fileprivate var DocumentUrlName = NSMutableArray() //for download file name
    fileprivate var DocumentID = NSMutableArray() //To identify the downloaded Doc
    //************ DocPic
    let documentInteractionController = UIDocumentInteractionController()
    
    
    //******* SearchBar
    var searchActive = false
    var filtered : NSMutableArray = NSMutableArray()
    
    //******* For Filter API
    fileprivate var sortFilterResponseArray = NSArray() // For Filter API Response
    var sortingApiState = true
    
    fileprivate var filterDocumentUrlArray = NSMutableArray() // For filter response Doc URL
    fileprivate var filterDocumentUrlName = NSMutableArray() //For download file name
    fileprivate var filterDocumentId = NSMutableArray() // To identify the download Doc
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sortingApiState = false
        
        printApiCall()
        
        sortView.isHidden = true
        btnSort.addTarget(self, action: #selector(btnSortTabbed), for: .touchUpInside)
        
        self.tableView.addSubview(self.refreshControl)
        documentInteractionController.delegate = self
        
        filterView.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        search.text = ""
        searchActive = false
        printApiCall()
        tableView.reloadData()
        
        sortingApiState = false
        
        docNameFiltr.text = ""
        manualFitrTxt.text = ""
        standardFiltrTxt.text = ""
        departmentFiltrTxt.text = ""
        
        docNameFiltr.resignFirstResponder()
        manualFitrTxt.resignFirstResponder()
        standardFiltrTxt.resignFirstResponder()
        departmentFiltrTxt.resignFirstResponder()
    }
    
    
    @IBAction func btnBackToHome(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //Header Sort Button
    @objc func btnSortTabbed() {
        
        //        if btnSort.isSelected == true {
        //            sortView.isHidden = false
        //            btnSort.isSelected = false
        //        }else {
        //            sortView.isHidden = true
        //            btnSort.isSelected = true
        //        }
        
        filterView.isHidden = false
        docNameFiltr.isHidden = false
        manualFitrTxt.isHidden = false
        standardFiltrTxt.isHidden = false
        departmentFiltrTxt.isHidden = false
    }
    
    @IBAction func btnCloseFilterview(_ sender: Any) {
        
        filterView.isHidden = true
        
        //        docNameFiltr.text = ""
        //        manualFitrTxt.text = ""
        //        standardFiltrTxt.text = ""
        //        departmentFiltrTxt.text = ""
        //
        //        docNameFiltr.resignFirstResponder()
        //        manualFitrTxt.resignFirstResponder()
        //        standardFiltrTxt.resignFirstResponder()
        //        departmentFiltrTxt.resignFirstResponder()
    }
    
    @IBAction func btnSubmitFilterView(_ sender: Any) {
        
        if docNameFiltr.text == "" && manualFitrTxt.text == "" && standardFiltrTxt.text == "" && departmentFiltrTxt.text == "" {
            
            sortingApiState = false
            print("All Fields are Nil")
            filterView.isHidden = true
        }
        else {
            
            filterView.isHidden = true  //
            sortingApiState = true
            
            filterApi()       //Call the filter API
            
            print("Fields are not Nil")
            
        }
        
    }
    
    
}


//MARK:- Custom Functions
//MARK:-
extension PrintVC {
    
    //Refresh Control.
    var refreshControl: UIRefreshControl {
        
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action: #selector(PrintVC.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor(red: 59/255, green: 208/255, blue: 214/255, alpha: 1.0)
        
        return refreshControl
        
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        if sortingApiState == true {
            
            self.filterDocumentUrlArray.removeAllObjects()
            self.filterDocumentUrlName.removeAllObjects()
            self.filterDocumentId.removeAllObjects()
            
            
            if docNameFiltr.text == "" && manualFitrTxt.text == "" && standardFiltrTxt.text == "" && departmentFiltrTxt.text == "" {
                sortingApiState = false
            }
            else {
                //Call the filter API Here
                filterApi()
            }
            
        }
        else {
            
            self.DocumentUrlArray.removeAllObjects()
            self.DocumentUrlName.removeAllObjects()
            self.DocumentID.removeAllObjects()
            
            //Call the Print API Here
            printApiCall()
        }
        
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    //Print Action Button of Table View.
    @objc func btnPrintAction(sender: UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        guard (tableView.cellForRow(at: indexPath) as? PrintViewCell) != nil else { return }
        
        print("THE INDEX PATH OF Print BUTTON", indexPath)
        
        if sortingApiState == false {
            
            // **** Main Response from Print Data Response API
            if searchActive == true {
                
                let dataDict = filtered.object(at: indexPath.row) as? NSDictionary ?? [:]
                
                if dataDict.value(forKey: "file") as? String == "" {
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "No Document Found", style: .error)
                    
                }
                else if dataDict.value(forKey: "file") as? String != "" {
                    storeAndShare(withURLString: dataDict.value(forKey: "file") as? String ?? "")
                }
                else {
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                }
                
            }
            else {
                
                if DocumentUrlArray[indexPath.row] as? String == "" {
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "No Document Found", style: .error)
                }
                else if DocumentUrlArray[indexPath.row] as? String != ""{
                    storeAndShare(withURLString: DocumentUrlArray[indexPath.row] as? String ?? "")
                }
                else {
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                }
                
            }
            
            
        }
            
        else {
            
            if searchActive == true {
                
                let dataDict = filtered.object(at: indexPath.row) as? NSDictionary ?? [:]
                
                if dataDict.value(forKey: "file") as? String == "" {
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "No Document Found", style: .error)
                }
                else if dataDict.value(forKey: "file") as? String != "" {
                    storeAndShare(withURLString: dataDict.value(forKey: "file") as? String ?? "")
                }
                else {
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                }
                
            }
            else {
                
                if filterDocumentUrlArray[indexPath.row] as? String == "" {
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "No Document Found", style: .error)
                }
                else if filterDocumentUrlArray[indexPath.row] as? String != "" {
                    storeAndShare(withURLString: filterDocumentUrlArray[indexPath.row] as? String ?? "")
                }
                else {
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
                }
                
            }
            
        }
        
    }
    
    //Download Action Button of Table View.
    @objc func btnDownloadAction(sender: UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        guard (tableView.cellForRow(at: indexPath) as? PrintViewCell) != nil else
        { return }
        
        print("THE INDEX PATH OF Download BUTTON", indexPath)
        
        if sortingApiState == false {
      
            //***** Main PrintScreen API
            
            if searchActive == true {
                let dataDict = filtered.object(at: indexPath.row) as? NSDictionary ?? [:]
                
                let docURL = dataDict.value(forKey: "file") as? String ?? ""
                let docID = dataDict.value(forKey: "id")
                
                let myNameString = docURL.components(separatedBy: "/").last
                
                if docURL == "" {
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "No Document Found", style: .error)
                }
                    
                else if docURL != "" {
                    downloadFileFromServer(fileURL: URL(string: docURL as? String ?? "")!, fileName: myNameString as? String ?? "", documentId: "\(docID!)" as? String ?? "")
                }
                
            }
                
            else {
                for (_, element) in DocumentUrlArray.enumerated() {
                    
                    let myString = "\(element)"
                    let seprateString = myString.components(separatedBy: "/").last
                    self.DocumentUrlName.add(seprateString!)
                }
                
                if DocumentUrlArray[indexPath.row] as? String == "" {
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "No Document Found", style: .error)
                }
                    
                else if DocumentUrlArray[indexPath.row] as? String != "" {
                    
                    downloadFileFromServer(fileURL: URL(string: DocumentUrlArray[indexPath.row] as? String ?? "")!, fileName: DocumentUrlName[indexPath.row] as? String ?? "", documentId: "\(DocumentID[indexPath.row])" as? String ?? "")
                }
                
            }
            
        }
        else {

            //***** For Filter API Result
            if searchActive == true {
                
                let dataDict = filtered.object(at: indexPath.row) as? NSDictionary ?? [:]
                let docURL = dataDict.value(forKey: "file") as? String ?? ""
                let docID = dataDict.value(forKey: "id")
                
                let myNameString = docURL.components(separatedBy: "/").last
                
                if docURL == "" {
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "No Document Found", style: .error)
                }
                else if docURL != "" {
                    downloadFileFromServer(fileURL: URL(string: docURL as? String ?? "")!, fileName: myNameString as? String ?? "", documentId: "\(docID!)" as? String ?? "")
                }
                
                
            }
            else {
                
                for (_, element) in filterDocumentUrlArray.enumerated() {
                    let myString = "\(element)"
                    let seprateString = myString.components(separatedBy: "/").last
                    self.filterDocumentUrlName.add(seprateString!)
                }
                
                if filterDocumentUrlArray[indexPath.row] as? String == "" {
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "No Document Found", style: .error)
                }
                    
                else if filterDocumentUrlArray[indexPath.row] as? String != "" {
                    
                    downloadFileFromServer(fileURL: URL(string: filterDocumentUrlArray[indexPath.row] as? String ?? "")!, fileName: filterDocumentUrlName[indexPath.row] as? String ?? "", documentId: "\(filterDocumentId[indexPath.row])" as? String ?? "")
                }
                
            }
            
            
        }
        
    }
    
    
}


//MARK:- Table View Functions
//MARK:-
extension PrintVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if sortingApiState == true {
            
            // For Filter API
            if searchActive == true {
                return filtered.count
            }
            else {
                return sortFilterResponseArray.count
            }
            
        }
        else {
            // For Print Main API
            if searchActive == true {
                return filtered.count
            }
                
            else {
                return self.printDataArray.count
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrintViewCell", for: indexPath) as! PrintViewCell
        
        if sortingApiState == false {
            
            if searchActive == true {
                
                let dataDict = filtered.object(at: indexPath.row) as? NSDictionary ?? [:]
                
                print("THE FILERED VAUE", filtered)
                cell.lblDocTitle.text = dataDict.value(forKey: "document_title") as? String ?? ""
                
                let responseImg = dataDict.value(forKey: "hospital_logo") as? String ?? ""
                cell.imgDocType.sd_setImage(with: URL(string: responseImg), placeholderImage: #imageLiteral(resourceName: "Default_logo"))
            }
                
            else {
                let dataDict = printDataArray.object(at: indexPath.row) as? NSDictionary ?? [:]
                
                cell.lblDocTitle.text = dataDict.value(forKey: "document_title") as? String ?? ""
                
                let responseImg = dataDict.value(forKey: "hospital_logo") as? String ?? ""
                cell.imgDocType.sd_setImage(with: URL(string: responseImg), placeholderImage: #imageLiteral(resourceName: "Default_logo"))
                
            }
            
        }
            
        else {
            
            if searchActive == true {
                let dataDict = filtered.object(at: indexPath.row) as? NSDictionary ?? [:]
                
                cell.lblDocTitle.text = dataDict.value(forKey: "document_title") as? String ?? ""
                let responseImg = dataDict.value(forKey: "hospital_logo") as? String ?? ""
                cell.imgDocType.sd_setImage(with: URL(string: responseImg), placeholderImage: #imageLiteral(resourceName: "Default_logo"))
                
            }
                
            else {
                
                if sortFilterResponseArray.count > 0 {
                    
                    let dataDict = sortFilterResponseArray.object(at: indexPath.row) as? NSDictionary ?? [:]
                    
                    cell.lblDocTitle.text = dataDict.value(forKey: "document_title") as? String ?? ""
                    let responseImg = dataDict.value(forKey: "hospital_logo") as? String ?? ""
                    cell.imgDocType.sd_setImage(with: URL(string: responseImg), placeholderImage: #imageLiteral(resourceName: "Default_logo"))
                    
                }
                else {
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "No data found", style: .error)
                }
                
            }
            
        }  // close Brace of sortApiState
        
        
        cell.btnPrint.addTarget(self, action: #selector(btnPrintAction), for: .touchUpInside)
        cell.btnPrint.tag = indexPath.row
        
        cell.btnDownload.addTarget(self, action: #selector(btnDownloadAction), for: .touchUpInside)
        cell.btnDownload.tag = indexPath.row
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 141
    }
    
    
}

//MARK:- Search Bar Delegate
//MARK:-
extension PrintVC: UISearchBarDelegate {
    
    // When button "Search" pressed on search Bar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.search.endEditing(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        if searchBar.text == "" {
            searchActive = false
        }
            
        else {
            searchActive = true
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        print("The enter text", searchText)
        filtered.removeAllObjects()
        
        if searchBar.text!.isEmpty {
            
            searchActive = false
        }
            
        else {
            
            searchActive = true
            
            if sortingApiState == false {
                
                if printDataArray.count >= 1 {
                    
                    for index in 0...printDataArray.count-1 {
                        
                        let dictResponse = printDataArray.object(at: index) as? NSDictionary ?? [:]
                        let documentName = dictResponse.object(forKey: "document_title") as? String ?? ""
                        
                        if (documentName.lowercased().range(of: searchText.lowercased()) != nil) {
                            
                            filtered.add(dictResponse)
                        }
                    }
                }
                
            }
            else {
                
                if sortFilterResponseArray.count >= 1 {
                    
                    for index in 0...sortFilterResponseArray.count-1 {
                        let dictResponse = sortFilterResponseArray.object(at: index) as? NSDictionary ?? [:]
                        let documentName = dictResponse.object(forKey: "document_title") as? String ?? ""
                        if (documentName.lowercased().range(of: searchText.lowercased()) != nil) {
                            filtered.add(dictResponse)
                        }
                    }
                }
                
            }
            
            
        }
        
        tableView.reloadData()
    }
    
    
}

//MARK:- Custom Functions
//MARK:-

extension PrintVC {
    
    func downloadFileFromServer(fileURL: URL, fileName: String, documentId: String) {
        
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        
        let destinationFileUrl = documentDirectory?.appendingPathComponent(fileName)
        
        let fileUrl = fileURL
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let request = URLRequest(url: fileUrl)
        
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            
            if let tempLocalUrl = tempLocalUrl, error == nil {
                
                //Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode{
                    print("Successfully downloaded. Status code: \(documentId)")
                    self.downloadedFiles(doc_id: documentId)
                }
                
                do{
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl!)
                    
                    print("THE TEMP Local URL", tempLocalUrl)
                    print("THE Destination URL", destinationFileUrl)
                }
                catch(let writeError) {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
                
            }
            else {
                print("Error took place while downloading a file. Error description: %@", error?.localizedDescription);
            }
        }
        task.resume()
        
        
    }
    
}

//MARK:- Web Services Api Call
//MARK:-
extension PrintVC{
    
    func printApiCall() {
        
        let passDict =  [
            "token":token
            ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            webServicesConnection.requestPOSTURL("printDocumentListing", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    let printDummyArray = self.printDataArray.mutableCopy() as? NSMutableArray ?? []
                    printDummyArray.removeAllObjects()
                    self.printDataArray = printDummyArray.mutableCopy() as? NSArray ?? []
                    
                    print("THE Print VC RESPONSE IS", responseJson)
                    
                    self.DocumentUrlArray.removeAllObjects()
                    self.DocumentID.removeAllObjects()
                    
                    self.printDataArray = responseJson["printDocumentListing"].arrayObject as? NSArray ?? []
                    self.tableView.reloadData()
                    
                    for item in self.printDataArray {
                        
                        let dataDict = item as? NSDictionary ?? [:]
                        
                        let documentString = dataDict.value(forKey: "file") as? String ?? ""
                        let documentId = dataDict.value(forKey: "id")
                        
                        self.DocumentUrlArray.add(documentString)
                        self.DocumentID.add(documentId)
                    }
                    print("THE URL ARRAY",self.DocumentUrlArray)
                    print("THE URL ID",self.DocumentID)
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                    Indicator.shared.hideProgressView()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                    let message = responseJson["message"].stringValue
                    Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                }
                
            }, failure: { (error) in
                
                Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
            
        else {
            
            Indicator.shared.hideProgressView()
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    //To DownLoad Doc
    func downloadedFiles(doc_id: String) {
        
        let passDict = ["token" : token,
                        "doc_id": doc_id
            ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            webServicesConnection.requestPOSTURL("download", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    print("THE Downloaded Doc RESPONSE IS", responseJson)
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: "Document Downloaded", style: .success)
                    
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                    let message = responseJson["message"].stringValue
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                }
                
            }, failure: { (error) in
                
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
            
        else {
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            self.dismiss(animated: true, completion: nil)
            
        }
        
    }
    
    func filterApi() {
        
        let passDict =  [ "token": token,
                          "document_name": docNameFiltr.text!,
                          "department": departmentFiltrTxt.text!] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            webServicesConnection.requestPOSTURL("filterDocument", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                
                if responseJson["status"].stringValue == "Success" {
                    
                    print("THE Filter Response Is", responseJson)
                    Indicator.shared.hideProgressView()
                    
                    let filterDummyArray = self.sortFilterResponseArray.mutableCopy() as? NSMutableArray ?? []
                    filterDummyArray.removeAllObjects()
                    self.sortFilterResponseArray = filterDummyArray.mutableCopy() as? NSArray ?? []
                    
                    self.filterDocumentId.removeAllObjects()  
                    self.filterDocumentUrlArray.removeAllObjects()
                    
                    self.sortFilterResponseArray = responseJson["filterDocument"].arrayObject as? NSArray ?? []
                    
                    print("THE RESPONCE ARRAY OF FILTER API", self.sortFilterResponseArray)
                    self.tableView.reloadData()
                    
                    for item in self.sortFilterResponseArray {
                        
                        let dataDict = item as? NSDictionary ?? [:]
                        let documentString = dataDict.value(forKey: "file") as? String ?? ""
                        let documentID = dataDict.value(forKey: "id")
                        
                        self.filterDocumentUrlArray.add(documentString)
                        self.filterDocumentId.add(documentID)
                    }
                    print("THE FILTER ARRAY URL", self.filterDocumentUrlArray)
                    print("THE FILTER URL ID", self.filterDocumentId)
                }
                    
                else if responseJson["message"].stringValue == "Invalid User." {
                    
                    Indicator.shared.hideProgressView()
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initLoginAtLogOut()
                    
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.USER_LOGIN_ANOTHER_DEVICE, style: .error)
                }
                    
                else {
                    
                    Indicator.shared.hideProgressView()
                    let message = responseJson["message"].stringValue
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                }
                
            }, failure: { (error) in
                Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
            
        }
        else {
            Indicator.shared.hideProgressView()
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
}

//MARK:- Document Interaction Controller
//MARK:-
extension PrintVC {
    /// This function will set all the required properties, and then provide a preview for the document
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.presentPreview(animated: true)
    }
    
    /// This function will store your document to some temporary URL and then provide sharing, copying, printing, saving options to the user
    func storeAndShare(withURLString: String) {
        guard let url = URL(string: withURLString) else { return }
        /// START YOUR ACTIVITY INDICATOR HERE
        Indicator.shared.showProgressView(self.view)
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else { return }
            let tmpURL = FileManager.default.temporaryDirectory
                .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
            do {
                try data.write(to: tmpURL)
            } catch {
                print(error)
            }
            DispatchQueue.main.async {
                /// STOP YOUR ACTIVITY INDICATOR HERE
                Indicator.shared.hideProgressView()
                self.share(url: tmpURL)
            }
            }.resume()
    }
}

extension PrintVC: UIDocumentInteractionControllerDelegate {
    /// If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}

extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}
