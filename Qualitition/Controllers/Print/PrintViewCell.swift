//
//  PrintViewCell.swift
//  Qualitition
//
//  Created by Callsoft on 12/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class PrintViewCell: UITableViewCell {

    
    @IBOutlet weak var imgDocType: UIImageView!
    @IBOutlet weak var lblDocTitle: UILabel!
    @IBOutlet weak var btnPrint: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
