//
//  GuestSignup.swift
//  Qualitition
//
//  Created by Callsoft on 10/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import UIKit

class GuestSignup: UIViewController, UITextFieldDelegate {

    //MARK:- IBOutlets
    //MARK:-
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var btnCheckImage: UIButton!
    
    
    //MARK:- Variables
    //MARK:-
    let validation:Validation = Validation.validationManager() as! Validation
    var selectedTermAndCondition: Bool = false
    var WebserviceConnection = AlmofireWrapper()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetupVC()
    }

    
    @IBAction func btnSignup(_ sender: Any) {
        validationSetup()
    }
    
    
    @IBAction func btnCheckTermCondition(_ sender: Any) {
        if selectedTermAndCondition == false {
            btnCheckImage.setImage(#imageLiteral(resourceName: "add_member_selected"), for: .normal)
            selectedTermAndCondition = true
        }
        else {
            btnCheckImage.setImage(#imageLiteral(resourceName: "un_selected_tick"), for: .normal)
            selectedTermAndCondition = false
        }
        
    }
    
    @IBAction func btnBackToSignUpType(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }

    
}


//MARK:- Custom Functions
//MARK:-
extension GuestSignup {
    
    //MARK:- Initial Set UP.
    //MARK:-
    func initialSetupVC() {
        
        let text = lblLogin.text
        let textRange = NSRange(location: 24, length: 6)
        let attributedText = NSMutableAttributedString(string: text!)
        
        attributedText.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 59.0/255, green: 208.0/255, blue: 214.0/255, alpha: 1.0), range: textRange)
        lblLogin.attributedText = attributedText
        
        
        lblLogin.isUserInteractionEnabled = true // Remember to do this
        let tapLblLogin: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(newToQaulitition))
        lblLogin.addGestureRecognizer(tapLblLogin)
        
        
        txtFirstName.delegate = self
        txtLastName.delegate = self
        
    }
    
    @objc func newToQaulitition(){
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.initLoginAtLogOut()
        
    }
    
        //For alphabets Only
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let texts = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ")
        
        if textField == txtFirstName
        {
            guard let text = txtFirstName.text else { return true }
            
            let newLength = text.characters.count + string.characters.count
                - range.length
            return newLength <= 15 && (txtFirstName.text! + string).rangeOfCharacter(from: texts.inverted) == nil
        }
        
        if textField == txtLastName
        {
            guard let text = txtLastName.text else { return true }
            
            let newLength = text.characters.count + string.characters.count
                - range.length
            return newLength <= 15 && (txtFirstName.text! + string).rangeOfCharacter(from: texts.inverted) == nil
        }
        
        return true
    }
    
    //MARK:- Validations
    //MARK:-
    func validationSetup()->Void{
        
        
        var message = ""
        
        if !validation.validateBlankField(txtFirstName.text!){
            
            message = "Please enter your First Name"
        }
        else if !validation.validateBlankField(txtLastName.text!) {
            
            message = "Please enter your Last Name"
        }
            
        else if !validation.validateBlankField(txtEmail.text!){
            message = "Please enter Email ID"
            
        }else if !validation.validateEmail(txtEmail.text!){
            
            message = "Please enter valid Email ID"
        }
            
        else if !validation.validateBlankField(txtPassword.text!){
            message = "Please enter Password"
        }
            
        else if !validation.validateBlankField(txtConfirmPassword.text!) {
            
            message = "Please enter Confirm Password"
        }
            
        else if txtPassword.text! != txtConfirmPassword.text!{
            
            message = "Password and Confirm Password must be same"
            
        }
            
        else if selectedTermAndCondition == false{
            message = "Please accept terms and conditions."
        }
        
        if message != "" {
            
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: AlertStyle.error)
            
        }else{
            signUp_ApiCall()
        }
        
    }
}


//MARK:- SIGN_UP_API CALL
//MARK:-
extension GuestSignup {
    
    func signUp_ApiCall() {
        
        let devicetoken = UserDefaults.standard.value(forKey: "devicetoken") as? String ?? "Simulaterwalatokenbb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326"
        
        let userType = UserDefaults.standard.value(forKey: "guestUser")
        
        let passDict = [
                            "first_name":txtFirstName.text!,
                            "last_name":txtLastName.text!,
                            "email": txtEmail.text!,
                            "password": txtPassword.text!,
                            "confirm_password": txtConfirmPassword.text!,
                            "type_user": userType ?? 51,
                            "devicetype":"iphone",
                            "devicetoken":devicetoken
            
            ] as [String : Any]
        
        if InternetConnection.internetshared.isConnectedToNetwork() {
            
            Indicator.shared.showProgressView(view)
            
            WebserviceConnection.requestPOSTURL("guestSignup", params: passDict as [String: AnyObject], headers: nil, success: { (responseJson) in
                
                
                if responseJson["status"].stringValue == "Success" {
                    
                    Indicator.shared.hideProgressView()
                    
                    print("Guest SignUp API Response",responseJson)
                    print("__________________________")
                    
                    let dataDict = responseJson["guestSignup"].dictionaryValue
                    let userName = dataDict["first_name"]?.stringValue as? String ?? ""
                    let userId = dataDict["id"]?.stringValue as? String ?? ""
                    let token = dataDict["token"]?.stringValue as? String ?? ""
                    
                    let userValue = 51
                    UserDefaults.standard.set(userValue, forKey: "userType") //overrite UserValue coming from Login Response
              
                    UserDefaults.standard.set(userName, forKey: "UserName")
                    
                    UserDefaults.standard.set("Guest", forKey: "UserRole") //overrite UserRole coming for backUp restriction //Doing that static
                    
                    UserDefaults.standard.set(userId, forKey: "ResponseUserID") //overrite UserId coming from Login
                    UserDefaults.standard.set(token, forKey: "responseToken") //overide the token from login to show at profile
                    
                    let appDel = UIApplication.shared.delegate as! AppDelegate
                    appDel.initHome()
                    
                    let successMessage = responseJson["message"].stringValue
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: successMessage, style: .success)

                }
                
                else {
                    
                    let message = responseJson["message"].stringValue
                    Indicator.shared.hideProgressView()
                    _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: message, style: .error)
                }
                
            }, failure: { (error) in
                
                    Indicator.shared.hideProgressView()
                _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_SOMETHING_WRONG, style: .error)
            })
        }
        
        else {
            
            Indicator.shared.hideProgressView()
            _ = SweetAlert().showAlert(ALERT_MESSAGE.ALERT_TITLE, subTitle: ALERT_MESSAGE.ALERT_NO_INTERNET, style: .error)
            self.dismiss(animated: true, completion: nil)
        }
    }

    
}
