//
//  HelperString.swift
//  Qualitition
//
//  Created by Callsoft on 18/09/18.
//  Copyright © 2018 Callsoft. All rights reserved.
//

import Foundation

class ALERT_MESSAGE {
    
    static var ALERT_TITLE = "Qualitition"
    static var ALERT_SOMETHING_WRONG = "Something Went Wrong, Please try Again"
    static var ALERT_NO_INTERNET = "No internet connection!"
    static var USER_LOGIN_ANOTHER_DEVICE = "You have logged in from another device, Please login again."
}

